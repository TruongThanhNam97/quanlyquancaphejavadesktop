/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.AccountDAL;
import DTO.Account;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class AccountBLL 
{
    
    private AccountBLL() 
    {
    }
    
    public static AccountBLL getInstance() 
    {
        return AccountBLLHolder.INSTANCE;
    }
    
    private static class AccountBLLHolder 
    {

        private static final AccountBLL INSTANCE = new AccountBLL();
    }
    public void insertAccount(String name,String passWord,boolean remember,String path)
    {
        AccountDAL.getInstance().insertAccount(name, passWord, remember, path);
    }
    public boolean isNameExit(String name)
    {
        return AccountDAL.getInstance().isNameExit(name);
    }
    public boolean isNameAndPassExit(String name,String pass)
    {
        return AccountDAL.getInstance().isNameAndPassExit(name, pass);
    }
    public void updateAccount(String name,String pass,String path)
    {
        AccountDAL.getInstance().updateAccount(name, pass, path);
    }
    public void updateAfterLogin(String name,boolean remember)
    {
        AccountDAL.getInstance().updateAfterLogin(name, remember);
    }
     public ArrayList<Account> displayCombox()
     {
         return AccountDAL.getInstance().displayCombox();
     }
     public Account getAccount(String nameAccount)
     {
         return AccountDAL.getInstance().getAccount(nameAccount);
     }
     public ArrayList<Account> displatToTable()
     {
         return AccountDAL.getInstance().displatToTable();
     }
     public File converByteArrayToFile(byte[] bytes)
     {
         return AccountDAL.getInstance().converByteArrayToFile(bytes);
     }
     public void deleteAccount(String name)
     {
         AccountDAL.getInstance().deleteAccount(name);
     }
}
