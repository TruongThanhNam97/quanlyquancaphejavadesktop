/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.BillDAL;
import DTO.Bill;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class BillBLL {
    
    private BillBLL() {
    }
    
    public static BillBLL getInstance() {
        return BillBLLHolder.INSTANCE;
    }
    
    private static class BillBLLHolder {

        private static final BillBLL INSTANCE = new BillBLL();
    }
    public void createBill(Date dateCheckIn,Date dateCheckOut,int idTable)
    {
        BillDAL.getInstance().createBill(dateCheckIn, dateCheckOut, idTable);
    }
    public int getIdBillForIdTableAndStatusBill(int idTable)
    {
        return BillDAL.getInstance().getIdBillForIdTableAndStatusBill(idTable);
    }
    public void updateTotalPrice(double totalPrice,int idBill)
    {
        BillDAL.getInstance().updateTotalPrice(totalPrice, idBill);
    }
    public void updateIdTable(int idBill,int idTable)
    {
        BillDAL.getInstance().updateIdTable(idBill, idTable);
    }
    public void deleteBill(int idBill)
    {
        BillDAL.getInstance().deleteBill(idBill);
    }
    public void updateTienNhanTienThuaSale(double tienNhan,double tienThua,int sale,int idBill)
    {
        BillDAL.getInstance().updateTienNhanTienThuaSale(tienNhan, tienThua, sale, idBill);
    }
    public void updateDateCheckOut(int idBill)
    {
        BillDAL.getInstance().updateDateCheckOut(idBill);
    }
    public void updateStatusBill(int idBill)
    {
        BillDAL.getInstance().updateStatusBill(idBill);
    }
    public double getTienNhan(int idBill)
    {
        return BillDAL.getInstance().getTienNhan(idBill);
    }
    public ArrayList<Bill> getListBillsByIdTable(int idTable)
    {
        return BillDAL.getInstance().getListBillsByIdTable(idTable);
    }
    public ArrayList<Timestamp> getTimestampByIdBill(int idBill)
    {
        return BillDAL.getInstance().getTimestampByIdBill(idBill);
    }
    public ArrayList<Bill> getListBillsByIdTableAndDate(int idTable,int day)
    {
        return BillDAL.getInstance().getListBillsByIdTableAndDate(idTable, day);
    }
    public ArrayList<Bill> getListBillsByIdTableAndMonth(int idTable,int month)
    {
        return BillDAL.getInstance().getListBillsByIdTableAndMonth(idTable, month);
    }
    public ArrayList<Bill> getListBillsByIdTableAndYear(int idTable,int year)
    {
        return BillDAL.getInstance().getListBillsByIdTableAndYear(idTable, year);
    }
    public ArrayList<Bill> getListBillsByIdTableAndDayMonthYear(int idTable,int day,int month,int year)
    {
        return BillDAL.getInstance().getListBillsByIdTableAndDayMonthYear(idTable, day, month, year);
    }
    public ArrayList<Bill> getListBillsByIdTableAndMonthYear(int idTable,int month,int year)
    {
        return BillDAL.getInstance().getListBillsByIdTableAndMonthYear(idTable, month, year);
    }
    public ArrayList<Bill> getListBillsByMonthYear(int month,int year)
    {
        return BillDAL.getInstance().getListBillsByMonthYear(month, year);
    }
    public ArrayList<Bill> getListBillsByYear(int year)
    {
        return BillDAL.getInstance().getListBillsByYear(year);
    }
    public ArrayList<Bill> getListBillsAll()
    {
        return BillDAL.getInstance().getListBillsAll();
    }
    public void updateDateCheckIn(int idBill)
    {
        BillDAL.getInstance().updateDateCheckIn(idBill);
    }
    public ArrayList<Bill> getListBillsByMonth(int month)
    {
        return BillDAL.getInstance().getListBillsByMonth(month);
    }
}
