/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.BillInfoDAL;
import DTO.BillInfo;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class BillInfoBLL {
    
    private BillInfoBLL() {
    }
    
    public static BillInfoBLL getInstance() {
        return BillInfoBLLHolder.INSTANCE;
    }
    
    private static class BillInfoBLLHolder {

        private static final BillInfoBLL INSTANCE = new BillInfoBLL();
    }
    public void createBillInfo(int idBill,int idFood,int count,String note)
    {
        BillInfoDAL.getInstance().createBillInfo(idBill, idFood, count, note);
    }
    public boolean isIdFoodExist(int idFood,int idBill)
    {
        return BillInfoDAL.getInstance().isIdFoodExist(idFood, idBill);
    }
    public int getIdBillInfoForIdBillAndIdFood(int idBill,int idFood)
    {
        return BillInfoDAL.getInstance().getIdBillInfoForIdBillAndIdFood(idBill, idFood);
    }
     public int getCountForIdBillAndIdFood(int idBill,int idFood)
     {
         return BillInfoDAL.getInstance().getCountForIdBillAndIdFood(idBill, idFood);
     }
     public void updateCountForIdBillInfo(int idBillInfo,int count)
     {
         BillInfoDAL.getInstance().updateCountForIdBillInfo(idBillInfo, count);
     }
     public ArrayList<BillInfo> getListBillInfoForIdBill(int idBill)
     {
         return BillInfoDAL.getInstance().getListBillInfoForIdBill(idBill);
     }
     public String getNoteIdBillAndIdFood(int idBill,int idFood)
     {
         return BillInfoDAL.getInstance().getNoteIdBillAndIdFood(idBill, idFood);
     }
      public void deleteBillInfo(int idBillInfo)
      {
          BillInfoDAL.getInstance().deleteBillInfo(idBillInfo);
      }
      public void updateIdBill(int idBill,int idBillInfo)
      {
         BillInfoDAL.getInstance().updateIdBill(idBill, idBillInfo);
      }
      public void updateNote(int idbillInfo,String note)
      {
          BillInfoDAL.getInstance().updateNote(idbillInfo, note);
      }
      public ArrayList<BillInfo> getListBillInfoesByIdFood(int idFood)
      {
          return BillInfoDAL.getInstance().getListBillInfoesByIdFood(idFood);
      }
}
