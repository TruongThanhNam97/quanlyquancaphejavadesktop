/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.*;
import java.sql.Connection;

/**
 *
 * @author Nam
 */
public class DatabaseBLL {
    public static void getInformation(String databaseName, String user, String pwd)
    {
        DatabaseDAL.getInformation(databaseName, user, pwd);
    }
    
    public static Connection getConnection()
    {
        return DatabaseDAL.getConnection();
    }
    
    public static boolean isConnect(String databaseName,String user,String pwd)
    {
        return DatabaseDAL.isConnect(databaseName, user, pwd);
    }
}
