/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.FoodDAL;
import DTO.Food;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class FoodBLL {
    
    private FoodBLL() {
    }
    
    public static FoodBLL getInstance() {
        return FoodBLLHolder.INSTANCE;
    }
    
    private static class FoodBLLHolder {

        private static final FoodBLL INSTANCE = new FoodBLL();
    }
    public int autoIdFood()
    {
        return FoodDAL.getInstance().autoIdFood();
    }
    public boolean isNameFoodExist(String nameFood)
    {
        return FoodDAL.getInstance().isNameFoodExist(nameFood);
    }
     public void createFood(String nameFood,int idFoodcategory,double price)
     {
         FoodDAL.getInstance().createFood(nameFood, idFoodcategory, price);
     }
     public ArrayList<Food> getListFoodForIdFoodcategory(int idFoodcategory)
     {
         return FoodDAL.getInstance().getListFoodForIdFoodcategory(idFoodcategory);
     }
      public String getNameFoodForIdFood(int idFood)
      {
          return FoodDAL.getInstance().getNameFoodForIdFood(idFood);
      }
      public double getPriceFoodForIdFood(int idFood)
      {
          return FoodDAL.getInstance().getPriceFoodForIdFood(idFood);
      }
        public int getIdFoodForNameFood(String nameFood)
        {
            return FoodDAL.getInstance().getIdFoodForNameFood(nameFood);
        }
        public void deleteFood(int idFood)
        {
            FoodDAL.getInstance().deleteFood(idFood);
        }
        public void updateFood(String nameFood,int idFoodcategory,double price,int idFood)
        {
            FoodDAL.getInstance().updateFood(nameFood, idFoodcategory, price, idFood);
        }
        public ArrayList<Food> getlistFoodsByNameFoodAndIdFoodcargory(String nameFood,int idFoodcategory)
        {
            return FoodDAL.getInstance().getlistFoodsByNameFoodAndIdFoodcargory(nameFood, idFoodcategory);
        }
        public ArrayList<Food> getlistFoodsByPriceFoodAndIdFoodcargory(String price,int idFoodcategory)
        {
            return FoodDAL.getInstance().getlistFoodsByPriceFoodAndIdFoodcargory(price, idFoodcategory);
        }
        public ArrayList<Food> getAllListFood()
        {
            return FoodDAL.getInstance().getAllListFood();
        }
}
