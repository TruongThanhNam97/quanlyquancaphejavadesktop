/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.FoodcategoryDAL;
import DTO.Foodcategory;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class FoodcategoryBLL {
    
    private FoodcategoryBLL() {
    }
    
    public static FoodcategoryBLL getInstance() {
        return FoodcategoryBLLHolder.INSTANCE;
    }
    
    private static class FoodcategoryBLLHolder {

        private static final FoodcategoryBLL INSTANCE = new FoodcategoryBLL();
    }
    public int autoIdFoodcategory()
    {
        return FoodcategoryDAL.getInstance().autoIdFoodcategory();
    }
     public boolean isNameFoodcategoryExist(String nameFoodcategory)
     {
         return FoodcategoryDAL.getInstance().isNameFoodcategoryExist(nameFoodcategory);
     }
     public void createFoodcategory(String nameFoodcategory)
     {
         FoodcategoryDAL.getInstance().createFoodcategory(nameFoodcategory);
     }
     public ArrayList<Foodcategory> displayCombobox()
     {
         return FoodcategoryDAL.getInstance().displayCombobox();
     }
      public int getIdFoodcategoryForNameFoodcategory(String nameFoodCategory)
      {
          return FoodcategoryDAL.getInstance().getIdFoodcategoryForNameFoodcategory(nameFoodCategory);
      }
      public void updateNameFoodcategory(String nameFoodcategory,int idFoodcategory)
      {
          FoodcategoryDAL.getInstance().updateNameFoodcategory(nameFoodcategory, idFoodcategory);
      }
      public void deleteFoodcategory(int idFoodcategory)
      {
          FoodcategoryDAL.getInstance().deleteFoodcategory(idFoodcategory);
      }
}
