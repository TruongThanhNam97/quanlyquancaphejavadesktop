/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.KhachHangDAL;
import DTO.KhachHang;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class KhachHangBLL {
    
    private KhachHangBLL() {
    }
    
    public static KhachHangBLL getInstance() {
        return KhachHangBLLHolder.INSTANCE;
    }
    
    private static class KhachHangBLLHolder {

        private static final KhachHangBLL INSTANCE = new KhachHangBLL();
    }
    public boolean isPhoneNumberExist(String phoneNumber)
    {
        return KhachHangDAL.getInstance().isPhoneNumberExist(phoneNumber);
    }
    public boolean isPhoneNumberAndPasswordExist(String phoneNumber,String passWord)
    {
        return KhachHangDAL.getInstance().isPhoneNumberAndPasswordExist(phoneNumber, passWord);
    }
    public void createKhachHang(String phoneNumber,String passWord,byte[] qrCode)
    {
        KhachHangDAL.getInstance().createKhachHang(phoneNumber, passWord, qrCode);
    }
    public int getIdKhachHangByPhoneNumber(String phoneNumber)
    {
        return KhachHangDAL.getInstance().getIdKhachHangByPhoneNumber(phoneNumber);
    }
    public int getScoreByIdKhachHang(int idKhachHang)
    {
        return KhachHangDAL.getInstance().getScoreByIdKhachHang(idKhachHang);
    }
    public void updateScoreByIdKhachHang(int idKhachHang)
    {
        KhachHangDAL.getInstance().updateScoreByIdKhachHang(idKhachHang);
    }
    public byte[] getQrCodeByPhoneNumber(String phoneNumber)
    {
        return KhachHangDAL.getInstance().getQrCodeByPhoneNumber(phoneNumber);
    }
    public KhachHang getKhachHangByIdKhachHang(int idKhachHang)
    {
        return KhachHangDAL.getInstance().getKhachHangByIdKhachHang(idKhachHang);
    }
    public ArrayList<KhachHang> getListKhachHang()
    {
        return KhachHangDAL.getInstance().getListKhachHang();
    }
    public ArrayList<KhachHang> getListKhachHangByPhoneNumber(String phoneNumber)
    {
        return KhachHangDAL.getInstance().getListKhachHangByPhoneNumber(phoneNumber);
    }
    public ArrayList<KhachHang> getListKhachHangByScore(int score)
    {
        return KhachHangDAL.getInstance().getListKhachHangByScore(score);
    }
}
