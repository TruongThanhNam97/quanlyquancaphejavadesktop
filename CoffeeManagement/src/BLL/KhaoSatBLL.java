/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.KhaoSatDAL;
import DTO.KhaoSat;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class KhaoSatBLL {
    
    private KhaoSatBLL() {
    }
    
    public static KhaoSatBLL getInstance() {
        return KhaoSatBLLHolder.INSTANCE;
    }
    
    private static class KhaoSatBLLHolder {

        private static final KhaoSatBLL INSTANCE = new KhaoSatBLL();
    }
    
    public ArrayList<KhaoSat> getDatas()
    {
        return KhaoSatDAL.getInstance().getDatas();
    }
}
