/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.TableDAL;
import DTO.Table;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class TableBLL {
    
    private TableBLL() {
    }
    
    public static TableBLL getInstance() {
        return TableBLLHolder.INSTANCE;
    }
    
    private static class TableBLLHolder {

        private static final TableBLL INSTANCE = new TableBLL();
    }
    public int autoIdTable()
    {
        return TableDAL.getInstance().autoIdTable();
    }
    public void addTable(String nameTable)
    {
        TableDAL.getInstance().addTable(nameTable);
    }
    public boolean isNameTableExist(String nameTable)
    {
        return TableDAL.getInstance().isNameTableExist(nameTable);
    }
    public ArrayList<Table> displayCombobox()
    {
        return TableDAL.getInstance().displayCombobox();
    }
    public ArrayList<Table> displayComboboxTableFrom()
    {
        return TableDAL.getInstance().displayComboboxTableFrom();
    }
    public ArrayList<Table> displayComboboxTableTo(int idTableFrom)
    {
        return TableDAL.getInstance().displayComboboxTableTo(idTableFrom);
    }
    public void updateStatusTable(String statusTable,int idTable)
    {
        TableDAL.getInstance().updateStatusTable(statusTable, idTable);
    }
    public Table getTableForIdTable(int idTable)
    {
        return TableDAL.getInstance().getTableForIdTable(idTable);
    }
    public int getIdTableByNameTable(String nameTable)
    {
        return TableDAL.getInstance().getIdTableByNameTable(nameTable);
    }
    public void updateNameTableByIdTable(int idTable,String nameTable)
    {
        TableDAL.getInstance().updateNameTableByIdTable(idTable, nameTable);
    }
    public void deleteTable(int idTable)
    {
        TableDAL.getInstance().deleteTable(idTable);
    }
     public ArrayList<Table> getListTablesByStatusTable(String statusTable)
     {
         return TableDAL.getInstance().getListTablesByStatusTable(statusTable);
     }
}
