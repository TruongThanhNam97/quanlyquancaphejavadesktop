/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.ThongKeFoodDAL;
import DTO.ThongKeFood;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class ThongKeFoodBLL {
    
    private ThongKeFoodBLL() {
    }
    
    public static ThongKeFoodBLL getInstance() {
        return ThongKeFoodBLLHolder.INSTANCE;
    }
    
    private static class ThongKeFoodBLLHolder {

        private static final ThongKeFoodBLL INSTANCE = new ThongKeFoodBLL();
    }
    public int autoIdThongKeFood()
    {
        return ThongKeFoodDAL.getInstance().autoIdThongKeFood();
    }
    public void insertData(int idFood,int countBillInfo)
    {
        ThongKeFoodDAL.getInstance().insertData(idFood, countBillInfo);
    }
    public void deleteData()
    {
        ThongKeFoodDAL.getInstance().deleteData();
    }
    public ArrayList<ThongKeFood> getData()
    {
        return ThongKeFoodDAL.getInstance().getData();
    }
}
