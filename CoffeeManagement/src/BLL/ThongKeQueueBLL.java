/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.ThongKeQueueDAL;
import DTO.ThongKeQueue;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class ThongKeQueueBLL {
    
    private ThongKeQueueBLL() {
    }
    
    public static ThongKeQueueBLL getInstance() {
        return ThongKeQueueBLLHolder.INSTANCE;
    }
    
    private static class ThongKeQueueBLLHolder {

        private static final ThongKeQueueBLL INSTANCE = new ThongKeQueueBLL();
    }
    
    public void insertData(String date,int timeQueue)
    {
        ThongKeQueueDAL.getInstance().insertData(date, timeQueue);
    }
    public void deleteData()
    {
        ThongKeQueueDAL.getInstance().deleteData();
    }
    public ArrayList<ThongKeQueue> getListData()
    {
        return ThongKeQueueDAL.getInstance().getListData();
    }
}
