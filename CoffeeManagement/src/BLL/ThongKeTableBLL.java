/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.ThongKeTableDAL;
import DTO.ThongKeTable;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class ThongKeTableBLL {
    
    private ThongKeTableBLL() {
    }
    
    public static ThongKeTableBLL getInstance() {
        return ThongKeTableBLLHolder.INSTANCE;
    }
    
    private static class ThongKeTableBLLHolder {

        private static final ThongKeTableBLL INSTANCE = new ThongKeTableBLL();
    }
    public int autoIdThongKeTable()
    {
        return ThongKeTableDAL.getInstance().autoIdThongKeTable();
    }
    public void insertData(int idTable,int countBill)
    {
        ThongKeTableDAL.getInstance().insertData(idTable, countBill);
    }
    public void deleteData()
    {
        ThongKeTableDAL.getInstance().deleteData();
    }
    public ArrayList<ThongKeTable> getData()
    {
        return ThongKeTableDAL.getInstance().getData();
    }
}
