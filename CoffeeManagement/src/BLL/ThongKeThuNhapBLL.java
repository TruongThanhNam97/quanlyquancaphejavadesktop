/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.ThongKeThuNhapDAL;
import DTO.ThongKeThuNhap;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class ThongKeThuNhapBLL {
    
    private ThongKeThuNhapBLL() {
    }
    
    public static ThongKeThuNhapBLL getInstance() {
        return ThongKeThuNhapBLLHolder.INSTANCE;
    }
    
    private static class ThongKeThuNhapBLLHolder {

        private static final ThongKeThuNhapBLL INSTANCE = new ThongKeThuNhapBLL();
    }
    
    public void insertData(String date,double thuNhap)
    {
        ThongKeThuNhapDAL.getInstance().insertData(date, thuNhap);
    }
    public void deleteData()
    {
        ThongKeThuNhapDAL.getInstance().deleteData();
    }
    public ArrayList<ThongKeThuNhap> getData()
    {
        return ThongKeThuNhapDAL.getInstance().getData();
    }
}
