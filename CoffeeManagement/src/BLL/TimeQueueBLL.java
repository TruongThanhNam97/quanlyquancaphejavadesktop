/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.TimeQueueDAL;
import DTO.TimeQueue;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class TimeQueueBLL {
    
    private TimeQueueBLL() {
    }
    
    public static TimeQueueBLL getInstance() {
        return TimeQueueBLLHolder.INSTANCE;
    }
    
    private static class TimeQueueBLLHolder {

        private static final TimeQueueBLL INSTANCE = new TimeQueueBLL();
    }
    public void insertData(int idTable,int timeQueue)
    {
        TimeQueueDAL.getInstance().insertData(idTable, timeQueue);
    }
    public void deleteData()
    {
        TimeQueueDAL.getInstance().deleteData();
    }
    public ArrayList<TimeQueue> getDatas()
    {
        return TimeQueueDAL.getInstance().getDatas();
    }
}
