/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DTO.Account;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Nam
 */
public class AccountDAL 
{
    
    private AccountDAL() 
    {
        
    }
    
    public static AccountDAL getInstance() 
    {
        return AccountDALHolder.INSTANCE;
    }
    
    private static class AccountDALHolder 
    {

        private static final AccountDAL INSTANCE = new AccountDAL();
    }
    
    private Connection con;
    
    private byte[] stringConvertByteArray(String path)
    {
        FileInputStream f = null;
        File file = new File(path);
        byte[] b = new byte[(int)file.length()];
        try 
        {
            f = new FileInputStream(file);
            f.read(b);
            f.close();
        } 
        catch (Exception e) 
        {
            b=null;
        }
        return b;
    }
    public File converByteArrayToFile(byte[] bytes)
    {
        File file = new File(System.getenv("localappdata")+"/image.txt");
        try
        {
            OutputStream os = new FileOutputStream(file);
            os.write(bytes);
            os.close();
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return file;
    }
    public void insertAccount(String name,String passWord,boolean remember,String path)
    {
        try
        {
          con = DatabaseDAL.getConnection();
          PreparedStatement pr = con.prepareStatement("insert into Account values(?,?,?,?)");
          pr.setString(1, name);
          pr.setString(2, passWord);
          pr.setBoolean(3, remember);
          pr.setBytes(4, stringConvertByteArray(path));
          pr.executeUpdate();
          pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public boolean isNameExit(String name)
    {
        con = DatabaseDAL.getConnection();
        try 
        {
            PreparedStatement pr = con.prepareStatement("select * from Account where name=?");
            pr.setString(1, name);
            ResultSet rs = pr.executeQuery();
            if(rs.next())
            {
                rs.close();
                pr.close();
                return true;
            }
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(AccountDAL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    public boolean isNameAndPassExit(String name,String pass)
    {
        con = DatabaseDAL.getConnection();
        try
        {
            PreparedStatement pr = con.prepareStatement("select * from Account where name=? and passWord=?");
            pr.setString(1, name);
            pr.setString(2, pass);
            ResultSet rs = pr.executeQuery();
            if(rs.next())
            {
                rs.close();
                pr.close();
                return true;
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return false;
    }
    public void updateAccount(String name,String pass,String path)
    {
        con = DatabaseDAL.getConnection();
        try
        {
            if(path.isEmpty())
            {
                PreparedStatement pr = con.prepareStatement("update Account set passWord=? where name=?");
                pr.setString(1, pass);
                pr.setString(2, name);
                pr.executeUpdate();
                pr.close();
            }
            else
            {
                PreparedStatement pr = con.prepareStatement("update Account set passWord=?,avatar=? where name=?");
                pr.setString(1, pass);
                pr.setBytes(2, stringConvertByteArray(path));
                pr.setString(3, name);
                pr.executeUpdate();
                pr.close();
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void updateAfterLogin(String name,boolean remember)
    {
        con = DatabaseDAL.getConnection();
        try
        {
            PreparedStatement pr = con.prepareStatement("update Account set remember=? where name=?");
            pr.setBoolean(1, remember);
            pr.setString(2, name);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public ArrayList<Account> displayCombox()
    {
       ArrayList<Account> list = new ArrayList<Account>();
       con = DatabaseDAL.getConnection();
       try
       {
           PreparedStatement pr = con.prepareStatement("select * from Account where remember=?");
           pr.setBoolean(1, true);
           ResultSet rs = pr.executeQuery();
           while(rs.next())
           {
               Account acc = new Account(rs.getString(1),rs.getString(2),rs.getBoolean(3),rs.getBytes(4));
               list.add(acc);
           }
           rs.close();
           pr.close();
       }
       catch(Exception ex)
       {
           ex.printStackTrace();
       }
       return list;
    }
    public Account getAccount(String nameAccount)
    {
      try
      {
          con = DatabaseDAL.getConnection();
          PreparedStatement pr = con.prepareStatement("select * from Account where name=?");
          pr.setString(1, nameAccount);
          ResultSet rs = pr.executeQuery();
          if (rs.next())
          {
              Account acc = new Account(rs.getString(1),rs.getString(2),rs.getBoolean(3),rs.getBytes(4));
              rs.close();
              pr.close();
              return acc;
          }
      }
      catch(Exception ex)
      {
          ex.printStackTrace();
      }
      return null;
    }
    public ArrayList<Account> displatToTable()
    {
        ArrayList<Account> listAccounts = new ArrayList<Account>();
        try
        {
            con = DatabaseDAL.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Account");
            while(rs.next())
            {
                listAccounts.add(new Account(rs.getString(1),rs.getString(2),rs.getBoolean(3),rs.getBytes(4)));
            }
            rs.close();
            stmt.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listAccounts;
    }
    public void deleteAccount(String name)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("delete from Account where name=?");
            pr.setString(1, name);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
