/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DTO.Bill;
import Format.FormatTypeData;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class BillDAL {
    private Connection con;
    private BillDAL() {
    }
    
    public static BillDAL getInstance() {
        return BillDALHolder.INSTANCE;
    }
    
    private static class BillDALHolder {

        private static final BillDAL INSTANCE = new BillDAL();
    }
    public int autoIdBill()
    {
        int id=0;
        try
        {
            con = DatabaseDAL.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Bill");
            while(rs.next())
            {
                id = rs.getInt(1);
            }
            rs.close();
            stmt.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return id+1;
    }
    public void createBill(Date dateCheckIn,Date dateCheckOut,int idTable)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("insert into Bill values(?,?,?,?,?,?,?,?,?)");
            pr.setInt(1, autoIdBill());
            pr.setTimestamp(2, new Timestamp(dateCheckIn.getTime()));
            pr.setTimestamp(3, new Timestamp(dateCheckOut.getTime()));
            pr.setInt(4, idTable);
            pr.setInt(5, 0);
            pr.setDouble(6, 0);
            pr.setDouble(7, 0);
            pr.setDouble(8, 0);
            pr.setInt(9, 0);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public int getIdBillForIdTableAndStatusBill(int idTable)
    {
        int id=0;
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from Bill where idTable=? and statusBill=?");
            pr.setInt(1, idTable);
            pr.setInt(2, 0);
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                id = rs.getInt(1);
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return id;
    }
    public void updateTotalPrice(double totalPrice,int idBill)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("update Bill set totalPrice=? where idBill=?");
            pr.setDouble(1,totalPrice);
            pr.setInt(2, idBill);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void updateIdTable(int idBill,int idTable)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("update Bill set idTable=? where idBill=?");
            pr.setInt(1, idTable);
            pr.setInt(2, idBill);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void deleteBill(int idBill)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("delete from Bill where idBill=?");
            pr.setInt(1, idBill);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void updateTienNhanTienThuaSale(double tienNhan,double tienThua,int sale,int idBill)
    {
        try
        {
           con = DatabaseDAL.getConnection();
           PreparedStatement pr = con.prepareStatement("update Bill set tienNhan=?,tienThua=?,sale=? where idBill=?");
           pr.setDouble(1, tienNhan);
           pr.setDouble(2, tienThua);
           pr.setInt(3, sale);
           pr.setInt(4, idBill);
           pr.executeUpdate();
           pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void updateDateCheckOut(int idBill)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("update Bill set dateCheckOut=? where idBill=?");
            pr.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
            pr.setInt(2, idBill);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void updateDateCheckIn(int idBill)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("update Bill set dateCheckIn=? where idBill=?");
            pr.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
            pr.setInt(2, idBill);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void updateStatusBill(int idBill)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("update Bill set statusBill=? where idBill=?");
            pr.setInt(1, 1);
            pr.setInt(2, idBill);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public double getTienNhan(int idBill)
    {
        double tienNhan=0;
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from Bill where idBill=?");
            pr.setInt(1, idBill);
            ResultSet rs = pr.executeQuery();
            if(rs.next())
            {
                tienNhan = rs.getDouble(7);
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return tienNhan;
    }
    public ArrayList<Bill> getListBillsByIdTable(int idTable)
    {
        ArrayList<Bill> listBills = new ArrayList<Bill>();
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from Bill where idTable=? and statusBill=?");
            pr.setInt(1, idTable);
            pr.setInt(2, 1);
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                listBills.add(new Bill(rs.getInt(1),rs.getDate(2),rs.getDate(3),rs.getInt(4),rs.getInt(5),rs.getDouble(6),rs.getDouble(7),rs.getDouble(8),rs.getInt(9)));
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listBills;
    }
    public ArrayList<Timestamp> getTimestampByIdBill(int idBill)
    {
        ArrayList<Timestamp> listTimestamps = new ArrayList<Timestamp>();
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from Bill where idBill=?");
            pr.setInt(1, idBill);
            ResultSet rs = pr.executeQuery();
            if (rs.next())
            {
                listTimestamps.add(rs.getTimestamp(2));
                listTimestamps.add(rs.getTimestamp(3));
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listTimestamps;
    }
    public ArrayList<Bill> getListBillsByIdTableAndDate(int idTable,int day)
    {
        ArrayList<Bill> listBills = new ArrayList<Bill>();
        try
        {
          con = DatabaseDAL.getConnection();
          PreparedStatement pr = con.prepareStatement("select * from Bill where idTable=? and statusBill=?");
          pr.setInt(1, idTable);
          pr.setInt(2, 1);
          ResultSet rs = pr.executeQuery();
          while(rs.next())
          {
             if (rs.getDate(3).getDate()==day)
             {
                 listBills.add(new Bill(rs.getInt(1),rs.getDate(2),rs.getDate(3),rs.getInt(4),rs.getInt(5),rs.getDouble(6),rs.getDouble(7),rs.getDouble(8),rs.getInt(9)));
             }
          }
          rs.close();
          pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listBills;
    }
    public ArrayList<Bill> getListBillsByIdTableAndMonth(int idTable,int month)
    {
        ArrayList<Bill> listBills = new ArrayList<Bill>();
        try
        {
          con = DatabaseDAL.getConnection();
          PreparedStatement pr = con.prepareStatement("select * from Bill where idTable=? and statusBill=?");
          pr.setInt(1, idTable);
          pr.setInt(2, 1);
          ResultSet rs = pr.executeQuery();
          while(rs.next())
          {
             if (rs.getDate(3).getMonth()+1==month)
             {
                 listBills.add(new Bill(rs.getInt(1),rs.getDate(2),rs.getDate(3),rs.getInt(4),rs.getInt(5),rs.getDouble(6),rs.getDouble(7),rs.getDouble(8),rs.getInt(9)));
             }
          }
          rs.close();
          pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listBills;
    }
    public ArrayList<Bill> getListBillsByIdTableAndYear(int idTable,int year)
    {
        ArrayList<Bill> listBills = new ArrayList<Bill>();
        try
        {
          con = DatabaseDAL.getConnection();
          PreparedStatement pr = con.prepareStatement("select * from Bill where idTable=? and statusBill=?");
          pr.setInt(1, idTable);
          pr.setInt(2, 1);
          ResultSet rs = pr.executeQuery();
          while(rs.next())
          {
             if (FormatTypeData.getInstance().formatYear(rs.getDate(3)).equals(year+""))
             {
                 listBills.add(new Bill(rs.getInt(1),rs.getDate(2),rs.getDate(3),rs.getInt(4),rs.getInt(5),rs.getDouble(6),rs.getDouble(7),rs.getDouble(8),rs.getInt(9)));
             }
          }
          rs.close();
          pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listBills;
    }
    public ArrayList<Bill> getListBillsByIdTableAndDayMonthYear(int idTable,int day,int month,int year)
    {
        ArrayList<Bill> listBills = new ArrayList<Bill>();
        try
        {
          con = DatabaseDAL.getConnection();
          PreparedStatement pr = con.prepareStatement("select * from Bill where idTable=? and statusBill=?");
          pr.setInt(1, idTable);
          pr.setInt(2, 1);
          ResultSet rs = pr.executeQuery();
          while(rs.next())
          {
             if (FormatTypeData.getInstance().formatYear(rs.getDate(3)).equals(year+"") && rs.getDate(3).getMonth()+1==month && rs.getDate(3).getDate()==day)
             {
                 listBills.add(new Bill(rs.getInt(1),rs.getDate(2),rs.getDate(3),rs.getInt(4),rs.getInt(5),rs.getDouble(6),rs.getDouble(7),rs.getDouble(8),rs.getInt(9)));
             }
          }
          rs.close();
          pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listBills;
    }
    public ArrayList<Bill> getListBillsByIdTableAndMonthYear(int idTable,int month,int year)
    {
        ArrayList<Bill> listBills = new ArrayList<Bill>();
        try
        {
          con = DatabaseDAL.getConnection();
          PreparedStatement pr = con.prepareStatement("select * from Bill where idTable=? and statusBill=?");
          pr.setInt(1, idTable);
          pr.setInt(2, 1);
          ResultSet rs = pr.executeQuery();
          while(rs.next())
          {
             if (FormatTypeData.getInstance().formatYear(rs.getDate(3)).equals(year+"") && rs.getDate(3).getMonth()+1==month)
             {
                 listBills.add(new Bill(rs.getInt(1),rs.getDate(2),rs.getDate(3),rs.getInt(4),rs.getInt(5),rs.getDouble(6),rs.getDouble(7),rs.getDouble(8),rs.getInt(9)));
             }
          }
          rs.close();
          pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listBills;
    }
    public ArrayList<Bill> getListBillsByMonthYear(int month,int year)
    {
        ArrayList<Bill> listBills = new ArrayList<Bill>();
        try
        {
          con = DatabaseDAL.getConnection();
          PreparedStatement pr = con.prepareStatement("select * from Bill where statusBill=?");
          pr.setInt(1, 1);
          ResultSet rs = pr.executeQuery();
          while(rs.next())
          {
             if (FormatTypeData.getInstance().formatYear(rs.getDate(3)).equals(year+"") && rs.getDate(3).getMonth()+1==month)
             {
                 listBills.add(new Bill(rs.getInt(1),rs.getDate(2),rs.getDate(3),rs.getInt(4),rs.getInt(5),rs.getDouble(6),rs.getDouble(7),rs.getDouble(8),rs.getInt(9)));
             }
          }
          rs.close();
          pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listBills;
    }
    public ArrayList<Bill> getListBillsByYear(int year)
    {
        ArrayList<Bill> listBills = new ArrayList<Bill>();
        try
        {
          con = DatabaseDAL.getConnection();
          PreparedStatement pr = con.prepareStatement("select * from Bill where statusBill=?");
          pr.setInt(1, 1);
          ResultSet rs = pr.executeQuery();
          while(rs.next())
          {
             if (FormatTypeData.getInstance().formatYear(rs.getDate(3)).equals(year+""))
             {
                 listBills.add(new Bill(rs.getInt(1),rs.getDate(2),rs.getDate(3),rs.getInt(4),rs.getInt(5),rs.getDouble(6),rs.getDouble(7),rs.getDouble(8),rs.getInt(9)));
             }
          }
          rs.close();
          pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listBills;
    }
    public ArrayList<Bill> getListBillsAll()
    {
        ArrayList<Bill> listBills = new ArrayList<Bill>();
        try
        {
          con = DatabaseDAL.getConnection();
          PreparedStatement pr = con.prepareStatement("select * from Bill where statusBill=?");
          pr.setInt(1, 1);
          ResultSet rs = pr.executeQuery();
          while(rs.next())
          {
                 listBills.add(new Bill(rs.getInt(1),rs.getDate(2),rs.getDate(3),rs.getInt(4),rs.getInt(5),rs.getDouble(6),rs.getDouble(7),rs.getDouble(8),rs.getInt(9)));
          }
          rs.close();
          pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listBills;
    }
    public ArrayList<Bill> getListBillsByMonth(int month)
    {
        ArrayList<Bill> listBills = new ArrayList<Bill>();
        try
        {
          con = DatabaseDAL.getConnection();
          PreparedStatement pr = con.prepareStatement("select * from Bill where statusBill=?");
          pr.setInt(1, 1);
          ResultSet rs = pr.executeQuery();
          while(rs.next())
          {
             if (rs.getDate(3).getMonth()+1==month)
             {
                 listBills.add(new Bill(rs.getInt(1),rs.getDate(2),rs.getDate(3),rs.getInt(4),rs.getInt(5),rs.getDouble(6),rs.getDouble(7),rs.getDouble(8),rs.getInt(9)));
             }
          }
          rs.close();
          pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listBills;
    }
}
