/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DTO.BillInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class BillInfoDAL {
    private Connection con;
    private BillInfoDAL() {
    }
    
    public static BillInfoDAL getInstance() {
        return BillInfoDALHolder.INSTANCE;
    }
    
    private static class BillInfoDALHolder {

        private static final BillInfoDAL INSTANCE = new BillInfoDAL();
    }
    public int autoIdBillInfo()
    {
        int id=0;
        try
        {
            con = DatabaseDAL.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from BillInfo");
            while(rs.next())
            {
                id = rs.getInt(1);
            }
            rs.close();
            stmt.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return id+1;
    }
    public void createBillInfo(int idBill,int idFood,int count,String note)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("insert into BillInfo values(?,?,?,?,?)");
            pr.setInt(1, autoIdBillInfo());
            pr.setInt(2, idBill);
            pr.setInt(3, idFood);
            pr.setInt(4, count);
            pr.setString(5, note);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public boolean isIdFoodExist(int idFood,int idBill)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from BillInfo where idBill=? and idFood=?");
            pr.setInt(1, idBill);
            pr.setInt(2, idFood);
            ResultSet rs = pr.executeQuery();
            if (rs.next())
            {
                rs.close();
                pr.close();
                return true;
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return false;
    }
    public int getIdBillInfoForIdBillAndIdFood(int idBill,int idFood)
    {
        int id=0;
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from BillInfo where idBill=? and idFood=?");
            pr.setInt(1, idBill);
            pr.setInt(2, idFood);
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                id = rs.getInt(1);
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return id;
    }
    public int getCountForIdBillAndIdFood(int idBill,int idFood)
    {
        int count=0;
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from BillInfo where idBill=? and idFood=?");
            pr.setInt(1, idBill);
            pr.setInt(2, idFood);
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                count = rs.getInt(4);
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return count;
    }
    public void updateCountForIdBillInfo(int idBillInfo,int count)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("update BillInfo set count=? where idBillInfo=?");
            pr.setInt(1, count);
            pr.setInt(2, idBillInfo);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public ArrayList<BillInfo> getListBillInfoForIdBill(int idBill)
    {
        ArrayList<BillInfo> ds = new ArrayList<BillInfo>();
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from BillInfo where idBill=?");
            pr.setInt(1, idBill);
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
             ds.add(new BillInfo(rs.getInt(1),rs.getInt(2), rs.getInt(3), rs.getInt(4),rs.getString(5)));
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return ds;
    }
    public String getNoteIdBillAndIdFood(int idBill,int idFood)
    {
        String note="";
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from BillInfo where idBill=? and idFood=?");
            pr.setInt(1, idBill);
            pr.setInt(2, idFood);
            ResultSet rs = pr.executeQuery();
            if(rs.next())
            {
                note = rs.getString(5);
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return note;
    }
    public void deleteBillInfo(int idBillInfo)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("delete from BillInfo where idBillInfo=?");
            pr.setInt(1, idBillInfo);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void updateIdBill(int idBill,int idBillInfo)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("update BillInfo set idBill=? where idBillInfo=?");
            pr.setInt(1, idBill);
            pr.setInt(2, idBillInfo);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void updateNote(int idbillInfo,String note)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("update BillInfo set note=? where idBillInfo=?");
            pr.setString(1, note);
            pr.setInt(2, idbillInfo);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public ArrayList<BillInfo> getListBillInfoesByIdFood(int idFood)
    {
        ArrayList<BillInfo> listBillInfoes = new ArrayList<BillInfo>();
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from BillInfo where idFood=?");
            pr.setInt(1, idFood);
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                listBillInfoes.add(new BillInfo(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getInt(4),rs.getString(5)));
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listBillInfoes;
    }
}
