/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Nam
 */
public class DatabaseDAL {

    private static String databaseName;
    private static String user;
    private static String pwd;
    private static Connection con;

    public static void getInformation(String databaseName, String user, String pwd) 
    {
        DatabaseDAL.databaseName = databaseName;
        DatabaseDAL.user = user;
        DatabaseDAL.pwd = pwd;
    }

    public static Connection getConnection() 
    {
        try
        {
            con = null;
            String uRL = "jdbc:sqlserver://127.0.0.1:1433;databaseName="+DatabaseDAL.databaseName+"";
            String user = DatabaseDAL.user;
            String pwd = DatabaseDAL.pwd;
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(uRL,user,pwd);
        }
        catch(Exception ex)
        {
            return null;
        }
        return con;
    }
    public static boolean isConnect(String databaseName,String user,String pwd)
    {
        try
        {
            con = null;
            String uRL = "jdbc:sqlserver://127.0.0.1:1433;databaseName="+databaseName+"";
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(uRL,user,pwd);
        }
        catch(Exception ex)
        {
            return false;
        }
        return true;
    }
}
