/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DTO.Food;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class FoodDAL {
    private Connection con;
    private FoodDAL() {
    }
    
    public static FoodDAL getInstance() {
        return FoodDALHolder.INSTANCE;
    }
    
    private static class FoodDALHolder {

        private static final FoodDAL INSTANCE = new FoodDAL();
    }
    public int autoIdFood()
    {
        int id=0;
        try
        {
            con = DatabaseDAL.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Food");
            while(rs.next())
                id = rs.getInt(1);
            rs.close();
            stmt.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return id+1;
    }
    public boolean isNameFoodExist(String nameFood)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from Food where nameFood=?");
            pr.setString(1, nameFood);
            ResultSet rs = pr.executeQuery();
            if(rs.next())
            {
                rs.close();
                pr.close();
                return true;
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return false;
    }
    public void createFood(String nameFood,int idFoodcategory,double price)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("insert into Food values(?,?,?,?)");
            pr.setInt(1, autoIdFood());
            pr.setString(2, nameFood);
            pr.setInt(3, idFoodcategory);
            pr.setDouble(4, price);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public ArrayList<Food> getListFoodForIdFoodcategory(int idFoodcategory)
    {
        ArrayList<Food> ds = new ArrayList<Food>();
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from Food where idFoodcategory=?");
            pr.setInt(1, idFoodcategory);
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                ds.add(new Food(rs.getInt(1),rs.getString(2),rs.getInt(3), rs.getDouble(4)));
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return ds;
    }
    public String getNameFoodForIdFood(int idFood)
    {
        String str = "";
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from Food where idFood=?");
            pr.setInt(1, idFood);
            ResultSet rs = pr.executeQuery();
            if(rs.next())
            {
                str = rs.getString(2);
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return str;
    }
    public double getPriceFoodForIdFood(int idFood)
    {
        double price=0;
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from Food where idFood=?");
            pr.setInt(1, idFood);
            ResultSet rs = pr.executeQuery();
            if (rs.next())
            {
                price = rs.getDouble(4);
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return price;
    }
    public int getIdFoodForNameFood(String nameFood)
    {
        int idFood=0;
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from Food where nameFood=?");
            pr.setString(1, nameFood);
            ResultSet rs = pr.executeQuery();
            if(rs.next())
            {
                idFood = rs.getInt(1);
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return idFood;
    }
    public void deleteFood(int idFood)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("delete from Food where idFood=?");
            pr.setInt(1, idFood);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void updateFood(String nameFood,int idFoodcategory,double price,int idFood)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("update Food set nameFood=?,idFoodcategory=?,price=? where idFood=?");
            pr.setString(1, nameFood);
            pr.setInt(2, idFoodcategory);
            pr.setDouble(3, price);
            pr.setInt(4, idFood);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public ArrayList<Food> getlistFoodsByNameFoodAndIdFoodcargory(String nameFood,int idFoodcategory)
    {
        ArrayList<Food> listFoods = new ArrayList<Food>();
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from Food where nameFood like ? and idFoodcategory=?");
            pr.setString(1, "%"+nameFood+"%");
            pr.setInt(2, idFoodcategory);
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                listFoods.add(new Food(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getDouble(4)));
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listFoods;
    }
    public ArrayList<Food> getlistFoodsByPriceFoodAndIdFoodcargory(String price,int idFoodcategory)
    {
        ArrayList<Food> listFoods = new ArrayList<Food>();
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from Food where price like ? and idFoodcategory=?");
            pr.setString(1, "%"+price+"%");
            pr.setInt(2, idFoodcategory);
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                listFoods.add(new Food(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getDouble(4)));
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listFoods;
    }
    public ArrayList<Food> getAllListFood()
    {
        ArrayList<Food> listFoods = new ArrayList<Food>();
        try
        {
            con = DatabaseDAL.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Food");
            while(rs.next())
            {
                listFoods.add(new Food(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getDouble(4)));
            }
            rs.close();
            stmt.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listFoods;
    }
}
