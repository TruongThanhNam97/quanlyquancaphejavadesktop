/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DTO.Foodcategory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class FoodcategoryDAL {
    private Connection con;
    private FoodcategoryDAL() {
    }
    
    public static FoodcategoryDAL getInstance() {
        return FoodcategoryDALHolder.INSTANCE;
    }
    
    private static class FoodcategoryDALHolder {

        private static final FoodcategoryDAL INSTANCE = new FoodcategoryDAL();
    }
    public int autoIdFoodcategory()
    {
        int id=0;
        try
        {
            con = DatabaseDAL.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Foodcategory");
            while(rs.next())
            {
                id = rs.getInt(1);
            }
            rs.close();
            stmt.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return id+1;
    }
    public boolean isNameFoodcategoryExist(String nameFoodcategory)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from Foodcategory where nameFoodcategory=?");
            pr.setString(1, nameFoodcategory);
            ResultSet rs = pr.executeQuery();
            if(rs.next())
            {
                rs.close();
                pr.close();
                return true;
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return false;
    }
    public void createFoodcategory(String nameFoodcategory)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("insert into Foodcategory values(?,?)");
            pr.setInt(1, autoIdFoodcategory());
            pr.setString(2, nameFoodcategory);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public ArrayList<Foodcategory> displayCombobox()
    {
        ArrayList<Foodcategory> ds = new ArrayList<Foodcategory>();
        try
        {
            con = DatabaseDAL.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Foodcategory");
            while(rs.next())
            {
                ds.add(new Foodcategory(rs.getInt(1),rs.getString(2)));
            }
            rs.close();
            stmt.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return ds;
    }
    public int getIdFoodcategoryForNameFoodcategory(String nameFoodCategory)
    {
        int id=0;
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from Foodcategory where nameFoodcategory=?");
            pr.setString(1, nameFoodCategory);
            ResultSet rs = pr.executeQuery();
            if(rs.next())
            {
                id = rs.getInt(1);
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return id;
    }
    public void updateNameFoodcategory(String nameFoodcategory,int idFoodcategory)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("update Foodcategory set nameFoodcategory=? where idFoodcategory=?");
            pr.setString(1, nameFoodcategory);
            pr.setInt(2, idFoodcategory);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void deleteFoodcategory(int idFoodcategory)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("delete from Foodcategory where idFoodcategory=?");
            pr.setInt(1, idFoodcategory);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
