/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DTO.KhachHang;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class KhachHangDAL {
    
    private KhachHangDAL() {
    }
    
    public static KhachHangDAL getInstance() {
        return KhachHangDALHolder.INSTANCE;
    }
    
    private static class KhachHangDALHolder {

        private static final KhachHangDAL INSTANCE = new KhachHangDAL();
    }
    private Connection con;
    private int autoIdKhachHang()
    {
        int id=0;
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from KhachHang");
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                id = rs.getInt(1);
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return id+1;
    }
    public boolean isPhoneNumberExist(String phoneNumber)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from KhachHang where phoneNumber=?");
            pr.setString(1, phoneNumber);
            ResultSet rs = pr.executeQuery();
            if(rs.next())
            {
                rs.close();
                pr.close();
                return true;
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return false;
    }
    public boolean isPhoneNumberAndPasswordExist(String phoneNumber,String passWord)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from KhachHang where phoneNumber=? and passWord=?");
            pr.setString(1, phoneNumber);
            pr.setString(2, passWord);
            ResultSet rs = pr.executeQuery();
            if(rs.next())
            {
                rs.close();
                pr.close();
                return true;
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return false;
    }
    public void createKhachHang(String phoneNumber,String passWord,byte[] qrCode)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("insert into KhachHang values(?,?,?,?,?)");
            pr.setInt(1, autoIdKhachHang());
            pr.setString(2, phoneNumber);
            pr.setString(3, passWord);
            pr.setInt(4, 0);
            pr.setBytes(5, qrCode);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public int getIdKhachHangByPhoneNumber(String phoneNumber)
    {
        int id=0;
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from KhachHang where phoneNumber=?");
            pr.setString(1, phoneNumber);
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                id = rs.getInt(1);
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return id;
    }
    public int getScoreByIdKhachHang(int idKhachHang)
    {
        int score=0;
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from KhachHang where idKhachHang=?");
            pr.setInt(1, idKhachHang);
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                score=rs.getInt(4);
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return score;
    }
    public void updateScoreByIdKhachHang(int idKhachHang)
    {
        try
        {
           int score = getScoreByIdKhachHang(idKhachHang);
           score+=25;
           con = DatabaseDAL.getConnection();
           PreparedStatement pr = con.prepareStatement("update KhachHang set score=? where idKhachHang=?");
           pr.setInt(1, score);
           pr.setInt(2, idKhachHang);
           pr.executeUpdate();
           pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public byte[] getQrCodeByPhoneNumber(String phoneNumber)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from KhachHang where phoneNumber=?");
            pr.setString(1, phoneNumber);
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                return rs.getBytes(5);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }
    public KhachHang getKhachHangByIdKhachHang(int idKhachHang)
    {
        KhachHang k = new KhachHang();
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from KhachHang where idKhachHang=?");
            pr.setInt(1, idKhachHang);
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                k.setIdKhachHang(rs.getInt(1));
                k.setPhoneNumber(rs.getString(2));
                k.setPassWord(rs.getString(3));
                k.setScore(rs.getInt(4));
                k.setQrCode(rs.getBytes(5));
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return k;
    }
    public ArrayList<KhachHang> getListKhachHang()
    {
        ArrayList<KhachHang> listDatas = new ArrayList<KhachHang>();
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from KhachHang order by score DESC");
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                listDatas.add(new KhachHang(rs.getInt(1),rs.getString(2),rs.getString(3), rs.getInt(4),rs.getBytes(5)));
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listDatas;
    }
    public ArrayList<KhachHang> getListKhachHangByPhoneNumber(String phoneNumber)
    {
        ArrayList<KhachHang> listDatas = new ArrayList<KhachHang>();
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from KhachHang where phoneNumber like ?");
            pr.setString(1, "%"+phoneNumber+"%");
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                listDatas.add(new KhachHang(rs.getInt(1),rs.getString(2),rs.getString(3), rs.getInt(4),rs.getBytes(5)));
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listDatas;
    }
    public ArrayList<KhachHang> getListKhachHangByScore(int score)
    {
        ArrayList<KhachHang> listDatas = new ArrayList<KhachHang>();
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from KhachHang where score >= ?");
            pr.setInt(1, score);
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                listDatas.add(new KhachHang(rs.getInt(1),rs.getString(2),rs.getString(3), rs.getInt(4),rs.getBytes(5)));
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listDatas;
    }
}
