/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DTO.KhaoSat;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class KhaoSatDAL {
    
    private KhaoSatDAL() {
    }
    
    public static KhaoSatDAL getInstance() {
        return KhaoSatDALHolder.INSTANCE;
    }
    
    private static class KhaoSatDALHolder {

        private static final KhaoSatDAL INSTANCE = new KhaoSatDAL();
    }
    private Connection con;
    
    public ArrayList<KhaoSat> getDatas()
    {
        ArrayList<KhaoSat> listDatas = new ArrayList<KhaoSat>();
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from KhaoSat");
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                listDatas.add(new KhaoSat(rs.getInt(1),rs.getInt(2),rs.getInt(3)));
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listDatas;
    }
}
