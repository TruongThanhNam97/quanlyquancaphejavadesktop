/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DTO.Table;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class TableDAL {
    
    private Connection con;
    private TableDAL() 
    {
        
    }
    
    public static TableDAL getInstance() 
    {
        return TableDALHolder.INSTANCE;
    }
    
    private static class TableDALHolder 
    {

        private static final TableDAL INSTANCE = new TableDAL();
    }
    
    public int autoIdTable()
    {
        int id=0;
        try
        {
            con = DatabaseDAL.getConnection();
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("select * from Tablee");
            while(rs.next())
            {
                id = rs.getInt(1);
            }
            rs.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return id+1;
    }
    public void addTable(String nameTable)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("insert into Tablee values(?,?,?)");
            pr.setInt(1, autoIdTable());
            pr.setString(2, nameTable);
            pr.setString(3, "Trống");
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public boolean isNameTableExist(String nameTable)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from Tablee where nameTable=?");
            pr.setString(1, nameTable);
            ResultSet rs = pr.executeQuery();
            if(rs.next())
            {
                rs.close();
                pr.close();
                return true;
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return false;
    }
    public ArrayList<Table> displayCombobox()
    {
        ArrayList<Table> ds = new ArrayList<Table>();
        try
        {
            con = DatabaseDAL.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from Tablee");
            while(rs.next())
            {
                ds.add(new Table(rs.getInt(1),rs.getString(2),rs.getString(3)));
            }
            rs.close();
            stmt.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return ds;
    }
    public ArrayList<Table> displayComboboxTableFrom()
    {
        ArrayList<Table> ds = new ArrayList<Table>();
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from Tablee where statusTable=?");
            pr.setString(1, "Đang chờ phục vụ - Ngồi quán");
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                ds.add(new Table(rs.getInt(1),rs.getString(2),rs.getString(3)));
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return ds;
    }
    public ArrayList<Table> displayComboboxTableTo(int idTableFrom)
    {
        ArrayList<Table> ds = new ArrayList<Table>();
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from Tablee where idTable!=? and statusTable=?");
            pr.setInt(1, idTableFrom);
            pr.setString(2, "Đang chờ phục vụ - Ngồi quán");
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                ds.add(new Table(rs.getInt(1),rs.getString(2),rs.getString(3)));
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return ds;
    }
    public void updateStatusTable(String statusTable,int idTable)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("update Tablee set statusTable=? where idTable=?");
            pr.setString(1, statusTable);
            pr.setInt(2, idTable);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public Table getTableForIdTable(int idTable)
    {
       try
       {
           con = DatabaseDAL.getConnection();
           PreparedStatement pr = con.prepareStatement("select * from Tablee where idTable=?");
           pr.setInt(1, idTable);
           ResultSet rs = pr.executeQuery();
           if (rs.next())
           {
               Table x = new Table(rs.getInt(1),rs.getString(2),rs.getString(3));
               rs.close();
               pr.close();
               return x;
           }
       }
       catch(Exception ex)
       {
           ex.printStackTrace();
       }
       return null;
    }
    public int getIdTableByNameTable(String nameTable)
    {
        int id=0;
        try
        {
           con = DatabaseDAL.getConnection();
           PreparedStatement pr = con.prepareStatement("select * from Tablee where nameTable=?");
           pr.setString(1, nameTable);
           ResultSet rs = pr.executeQuery();
           if(rs.next())
           {
               id = rs.getInt(1);
           }
           rs.close();
           pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return id;
    }
    public void updateNameTableByIdTable(int idTable,String nameTable)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("update Tablee set nameTable=? where idTable=?");
            pr.setString(1, nameTable);
            pr.setInt(2, idTable);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void deleteTable(int idTable)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("delete from Tablee where idTable=?");
            pr.setInt(1, idTable);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public ArrayList<Table> getListTablesByStatusTable(String statusTable)
    {
        ArrayList<Table> listTables = new ArrayList<Table>();
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from Tablee where statusTable=?");
            pr.setString(1,statusTable);
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                listTables.add(new Table(rs.getInt(1),rs.getString(2), rs.getString(3)));
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listTables;
    }
}
