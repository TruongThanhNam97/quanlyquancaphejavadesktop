/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DTO.ThongKeFood;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class ThongKeFoodDAL {
    
    private ThongKeFoodDAL() {
    }
    
    public static ThongKeFoodDAL getInstance() {
        return ThongKeFoodDALHolder.INSTANCE;
    }
    
    private static class ThongKeFoodDALHolder {

        private static final ThongKeFoodDAL INSTANCE = new ThongKeFoodDAL();
    }
    
    private Connection con;
    public int autoIdThongKeFood()
    {
        int id=0;
        try
        {
            con = DatabaseDAL.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from ThongKeFood");
            while(rs.next())
            {
                id = rs.getInt(1);
            }
            rs.close();
            stmt.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return id+1;
    }
    public void insertData(int idFood,int countBillInfo)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("insert into ThongKeFood values(?,?,?)");
            pr.setInt(1, autoIdThongKeFood());
            pr.setInt(2, idFood);
            pr.setInt(3, countBillInfo);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void deleteData()
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("delete from ThongKeFood");
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public ArrayList<ThongKeFood> getData()
    {
        ArrayList<ThongKeFood> listDatas = new ArrayList<ThongKeFood>();
        try
        {
            con = DatabaseDAL.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from ThongKeFood order by countBillInfo DESC");
            while(rs.next())
            {
                listDatas.add(new ThongKeFood(rs.getInt(1),rs.getInt(2),rs.getInt(3)));
            }
            rs.close();
            stmt.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listDatas;
    }
}
