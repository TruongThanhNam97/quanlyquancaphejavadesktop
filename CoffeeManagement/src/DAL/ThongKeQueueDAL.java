/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DTO.ThongKeQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class ThongKeQueueDAL {
    
    private ThongKeQueueDAL() {
    }
    
    public static ThongKeQueueDAL getInstance() {
        return ThongKeQueueDALHolder.INSTANCE;
    }
    
    private static class ThongKeQueueDALHolder {

        private static final ThongKeQueueDAL INSTANCE = new ThongKeQueueDAL();
    }
    
    private Connection con;
    
    private int autoIdThongKeQueue()
    {
        int id=0;
        try
        {
            con = DatabaseDAL.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from ThongKeQueue");
            while(rs.next())
            {
                id = rs.getInt(1);
            }
            rs.close();
            stmt.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return id+1;
    }
    public void insertData(String date,int timeQueue)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("insert into ThongKeQueue values(?,?,?)");
            pr.setInt(1, autoIdThongKeQueue());
            pr.setString(2, date);
            pr.setInt(3, timeQueue);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void deleteData()
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("delete from ThongKeQueue");
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public ArrayList<ThongKeQueue> getListData()
    {
        ArrayList<ThongKeQueue> listDatas = new ArrayList<ThongKeQueue>();
        try
        {
            con = DatabaseDAL.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from ThongKeQueue order by timeQueue DESC");
            while(rs.next())
            {
                listDatas.add(new ThongKeQueue(rs.getInt(1),rs.getString(2),rs.getInt(3)));
            }
            rs.close();
            stmt.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listDatas;
    }
}
