/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DTO.ThongKeTable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class ThongKeTableDAL {
    
    private ThongKeTableDAL() {
    }
    
    public static ThongKeTableDAL getInstance() {
        return ThongKeTableDALHolder.INSTANCE;
    }
    
    private static class ThongKeTableDALHolder {

        private static final ThongKeTableDAL INSTANCE = new ThongKeTableDAL();
    }
    
    private Connection con;
    
    public int autoIdThongKeTable()
    {
        int id=0;
        try
        {
            con = DatabaseDAL.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from ThongKeTable");
            while(rs.next())
            {
                id = rs.getInt(1);
            }
            rs.close();
            stmt.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return id+1;
    }
    public void insertData(int idTable,int countBill)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("insert into ThongKeTable values(?,?,?)");
            pr.setInt(1, autoIdThongKeTable());
            pr.setInt(2, idTable);
            pr.setInt(3, countBill);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void deleteData()
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("delete from ThongKeTable");
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public ArrayList<ThongKeTable> getData()
    {
        ArrayList<ThongKeTable> listDatas = new ArrayList<ThongKeTable>();
        try
        {
            con = DatabaseDAL.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from ThongKeTable order by countBill DESC");
            while(rs.next())
            {
                listDatas.add(new ThongKeTable(rs.getInt(1),rs.getInt(2),rs.getInt(3)));
            }
            rs.close();
            stmt.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listDatas;
    }
}
