/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DTO.ThongKeThuNhap;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class ThongKeThuNhapDAL {
    
    private ThongKeThuNhapDAL() {
    }
    
    public static ThongKeThuNhapDAL getInstance() {
        return ThongKeThuNhapDALHolder.INSTANCE;
    }
    
    private static class ThongKeThuNhapDALHolder {

        private static final ThongKeThuNhapDAL INSTANCE = new ThongKeThuNhapDAL();
    }
    
    private Connection con;
    private int autoIdThongKeThuNhap()
    {
        int id=0;
        try
        {
            con = DatabaseDAL.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from ThongKeThuNhap");
            while(rs.next())
            {
                id = rs.getInt(1);
            }
            rs.close();
            stmt.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return id+1;
    }
    public void insertData(String date,double thuNhap)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr= con.prepareStatement("insert into ThongKeThuNhap values(?,?,?)");
            pr.setInt(1, autoIdThongKeThuNhap());
            pr.setString(2, date);
            pr.setDouble(3, thuNhap);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void deleteData()
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("delete from ThongKeThuNhap");
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public ArrayList<ThongKeThuNhap> getData()
    {
        ArrayList<ThongKeThuNhap> listDatas =new ArrayList<ThongKeThuNhap>();
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from ThongKeThuNhap order by thuNhap DESC");
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                listDatas.add(new ThongKeThuNhap(rs.getInt(1),rs.getString(2), rs.getDouble(3)));
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listDatas;
    }
}
