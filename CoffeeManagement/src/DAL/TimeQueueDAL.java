/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DTO.TimeQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class TimeQueueDAL {
    
    private TimeQueueDAL() {
    }
    
    public static TimeQueueDAL getInstance() {
        return TimeQueueDALHolder.INSTANCE;
    }
    
    private static class TimeQueueDALHolder {

        private static final TimeQueueDAL INSTANCE = new TimeQueueDAL();
    }
    
    private Connection con;
    
    private int autoIdQueue()
    {
        int id=0;
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from TimeQueue");
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                id = rs.getInt(1);
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return id+1;
    }
    public void insertData(int idTable,int timeQueue)
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("insert into TimeQueue values(?,?,?)");
            pr.setInt(1, autoIdQueue());
            pr.setInt(2, idTable);
            pr.setInt(3, timeQueue);
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void deleteData()
    {
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("delete from TimeQueue");
            pr.executeUpdate();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public ArrayList<TimeQueue> getDatas()
    {
        ArrayList<TimeQueue> listDatas = new ArrayList<TimeQueue>();
        try
        {
            con = DatabaseDAL.getConnection();
            PreparedStatement pr = con.prepareStatement("select * from TimeQueue order by timeQueue DESC");
            ResultSet rs = pr.executeQuery();
            while(rs.next())
            {
                listDatas.add(new TimeQueue(rs.getInt(1),rs.getInt(2), rs.getInt(3)));
            }
            rs.close();
            pr.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return listDatas;
    }
}
