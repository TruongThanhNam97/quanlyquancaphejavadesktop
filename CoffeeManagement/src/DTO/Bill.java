/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import Format.FormatTypeData;
import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class Bill 
{
    private int idBill;
    private Date dateCheckIn;
    private Date dateCheckOut;
    private Table table;
    private int idTable;
    private int statusBill;
    private double totalPrice;
    private double tienNhan;
    private double tienThua;
    private int sale;
    private ArrayList<BillInfo> dsBillIno = new ArrayList<BillInfo>();

    public Bill() {
    }

    public Bill(int idBill, Date dateCheckIn, Date dateCheckOut, int idTable, int statusBill, double totalPrice, double tienNhan, double tienThua, int sale) {
        this.idBill = idBill;
        this.dateCheckIn = dateCheckIn;
        this.dateCheckOut = dateCheckOut;
        this.idTable = idTable;
        this.statusBill = statusBill;
        this.totalPrice = totalPrice;
        this.tienNhan = tienNhan;
        this.tienThua = tienThua;
        this.sale = sale;
    }

    public int getIdBill() {
        return idBill;
    }

    public void setIdBill(int idBill) {
        this.idBill = idBill;
    }

    public Date getDateCheckIn() {
        return dateCheckIn;
    }

    public void setDateCheckIn(Date dateCheckIn) {
        this.dateCheckIn = dateCheckIn;
    }

    public Date getDateCheckOut() {
        return dateCheckOut;
    }

    public void setDateCheckOut(Date dateCheckOut) {
        this.dateCheckOut = dateCheckOut;
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public int getIdTable() {
        return idTable;
    }

    public void setIdTable(int idTable) {
        this.idTable = idTable;
    }

    public int getStatusBill() {
        return statusBill;
    }

    public void setStatusBill(int statusBill) {
        this.statusBill = statusBill;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getTienNhan() {
        return tienNhan;
    }

    public void setTienNhan(double tienNhan) {
        this.tienNhan = tienNhan;
    }

    public double getTienThua() {
        return tienThua;
    }

    public void setTienThua(double tienThua) {
        this.tienThua = tienThua;
    }

    public int getSale() {
        return sale;
    }

    public void setSale(int sale) {
        this.sale = sale;
    }

    public ArrayList<BillInfo> getDsBillIno() {
        return dsBillIno;
    }

    public void setDsBillIno(ArrayList<BillInfo> dsBillIno) {
        this.dsBillIno = dsBillIno;
    }

    @Override
    public String toString() {
        return "Bill - "+FormatTypeData.getInstance().formatDateTime(this.dateCheckOut);
    }
    
}
