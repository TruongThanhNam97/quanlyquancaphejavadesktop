/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Nam
 */
public class BillInfo 
{
    private int idBillInfo;
    private Bill bill;
    private int idBill;
    private Food food;
    private int idFood;
    private int count;
    private String note;

    public BillInfo() {
    }

    public BillInfo(int idBillInfo, int idBill, int idFood, int count, String note) {
        this.idBillInfo = idBillInfo;
        this.idBill = idBill;
        this.idFood = idFood;
        this.count = count;
        this.note = note;
    }

    public int getIdBillInfo() {
        return idBillInfo;
    }

    public void setIdBillInfo(int idBillInfo) {
        this.idBillInfo = idBillInfo;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public int getIdBill() {
        return idBill;
    }

    public void setIdBill(int idBill) {
        this.idBill = idBill;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public int getIdFood() {
        return idFood;
    }

    public void setIdFood(int idFood) {
        this.idFood = idFood;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
    
}
