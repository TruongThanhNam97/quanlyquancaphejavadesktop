/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class Food 
{
    private int idFood;
    private String nameFood;
    private Foodcategory foodcategory;
    private int idFoodcategory;
    private double price;
    private ArrayList<BillInfo> dsBillInfo = new ArrayList<BillInfo>();

    public Food() {
    }

    public Food(int idFood, String nameFood, int idFoodcategory, double price) {
        this.idFood = idFood;
        this.nameFood = nameFood;
        this.idFoodcategory = idFoodcategory;
        this.price = price;
    }

    public int getIdFood() {
        return idFood;
    }

    public void setIdFood(int idFood) {
        this.idFood = idFood;
    }

    public String getNameFood() {
        return nameFood;
    }

    public void setNameFood(String nameFood) {
        this.nameFood = nameFood;
    }

    public Foodcategory getFoodcategory() {
        return foodcategory;
    }

    public void setFoodcategory(Foodcategory foodcategory) {
        this.foodcategory = foodcategory;
    }

    public int getIdFoodcategory() {
        return idFoodcategory;
    }

    public void setIdFoodcategory(int idFoodcategory) {
        this.idFoodcategory = idFoodcategory;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ArrayList<BillInfo> getDsBillInfo() {
        return dsBillInfo;
    }

    public void setDsBillInfo(ArrayList<BillInfo> dsBillInfo) {
        this.dsBillInfo = dsBillInfo;
    }
    
    @Override
    public String toString() 
    {
        return this.nameFood;
    }
    
}
