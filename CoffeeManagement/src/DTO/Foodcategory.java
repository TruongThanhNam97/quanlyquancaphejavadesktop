/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class Foodcategory 
{
    private int idFoodcategory;
    private String nameFoodcategory;
    private ArrayList<Food> dsFood = new ArrayList<Food>();

    public Foodcategory() {
    }

    public Foodcategory(int idFoodcategory, String nameFoodcategory) {
        this.idFoodcategory = idFoodcategory;
        this.nameFoodcategory = nameFoodcategory;
    }

    public int getIdFoodcategory() {
        return idFoodcategory;
    }

    public void setIdFoodcategory(int idFoodcategory) {
        this.idFoodcategory = idFoodcategory;
    }

    public String getNameFoodcategory() {
        return nameFoodcategory;
    }

    public void setNameFoodcategory(String nameFoodcategory) {
        this.nameFoodcategory = nameFoodcategory;
    }

    public ArrayList<Food> getDsFood() {
        return dsFood;
    }

    public void setDsFood(ArrayList<Food> dsFood) {
        this.dsFood = dsFood;
    }
    
    @Override
    public String toString() 
    {
        return this.nameFoodcategory;
    }
    
}
