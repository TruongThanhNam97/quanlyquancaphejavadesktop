/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Nam
 */
public class KhachHang {
    private int idKhachHang;
    private String phoneNumber;
    private String passWord;
    private int score;
    private byte[] qrCode;

    public KhachHang() {
    }

    public KhachHang(int idKhachHang, String phoneNumber, String passWord, int score, byte[] qrCode) {
        this.idKhachHang = idKhachHang;
        this.phoneNumber = phoneNumber;
        this.passWord = passWord;
        this.score = score;
        this.qrCode = qrCode;
    }

    public int getIdKhachHang() {
        return idKhachHang;
    }

    public void setIdKhachHang(int idKhachHang) {
        this.idKhachHang = idKhachHang;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public byte[] getQrCode() {
        return qrCode;
    }

    public void setQrCode(byte[] qrCode) {
        this.qrCode = qrCode;
    }
    
}
