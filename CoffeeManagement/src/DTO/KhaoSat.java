/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Nam
 */
public class KhaoSat 
{
    private int idKhaoSat;
    private int quyUoc;
    private int idKhachHang;

    public KhaoSat() {
    }

    public KhaoSat(int idKhaoSat, int quyUoc, int idKhachHang) {
        this.idKhaoSat = idKhaoSat;
        this.quyUoc = quyUoc;
        this.idKhachHang = idKhachHang;
    }

    public int getIdKhaoSat() {
        return idKhaoSat;
    }

    public void setIdKhaoSat(int idKhaoSat) {
        this.idKhaoSat = idKhaoSat;
    }

    public int getQuyUoc() {
        return quyUoc;
    }

    public void setQuyUoc(int quyUoc) {
        this.quyUoc = quyUoc;
    }

    public int getIdKhachHang() {
        return idKhachHang;
    }

    public void setIdKhachHang(int idKhachHang) {
        this.idKhachHang = idKhachHang;
    }

    
}
