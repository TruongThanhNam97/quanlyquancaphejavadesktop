/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.ArrayList;

/**
 *
 * @author Nam
 */
public class Table 
{
    private int idTable;
    private String nameTable;
    private String statusTable;
    private ArrayList<Bill> dsBill = new ArrayList<Bill>();

    public Table() {
    }

    public Table(int idTable, String nameTable, String statusTable) {
        this.idTable = idTable;
        this.nameTable = nameTable;
        this.statusTable = statusTable;
    }

    public int getIdTable() {
        return idTable;
    }

    public void setIdTable(int idTable) {
        this.idTable = idTable;
    }

    public String getNameTable() {
        return nameTable;
    }

    public void setNameTable(String nameTable) {
        this.nameTable = nameTable;
    }

    public String getStatusTable() {
        return statusTable;
    }

    public void setStatusTable(String statusTable) {
        this.statusTable = statusTable;
    }

    public ArrayList<Bill> getDsBill() {
        return dsBill;
    }

    public void setDsBill(ArrayList<Bill> dsBill) {
        this.dsBill = dsBill;
    }

    @Override
    public String toString() {
        return this.nameTable+" - "+this.statusTable;
    }
    
}
