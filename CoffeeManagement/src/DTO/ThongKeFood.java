/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Nam
 */
public class ThongKeFood 
{
    private int idThongKeFood;
    private int idFood;
    private int countBillInfo;

    public ThongKeFood() {
    }

    public ThongKeFood(int idThongKeFood, int idFood, int countBillInfo) {
        this.idThongKeFood = idThongKeFood;
        this.idFood = idFood;
        this.countBillInfo = countBillInfo;
    }

    public int getIdThongKeFood() {
        return idThongKeFood;
    }

    public void setIdThongKeFood(int idThongKeFood) {
        this.idThongKeFood = idThongKeFood;
    }

    public int getIdFood() {
        return idFood;
    }

    public void setIdFood(int idFood) {
        this.idFood = idFood;
    }

    public int getCountBillInfo() {
        return countBillInfo;
    }

    public void setCountBillInfo(int countBillInfo) {
        this.countBillInfo = countBillInfo;
    }
    
}
