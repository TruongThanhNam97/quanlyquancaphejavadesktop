/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Nam
 */
public class ThongKeQueue 
{
    private int idThongKeQueue;
    private String date;
    private int timeQueue;

    public ThongKeQueue() {
    }

    public ThongKeQueue(int idThongKeQueue, String date, int timeQueue) {
        this.idThongKeQueue = idThongKeQueue;
        this.date = date;
        this.timeQueue = timeQueue;
    }

    public int getIdThongKeQueue() {
        return idThongKeQueue;
    }

    public void setIdThongKeQueue(int idThongKeQueue) {
        this.idThongKeQueue = idThongKeQueue;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getTimeQueue() {
        return timeQueue;
    }

    public void setTimeQueue(int timeQueue) {
        this.timeQueue = timeQueue;
    }
    
}
