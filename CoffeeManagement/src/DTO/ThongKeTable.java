/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Nam
 */
public class ThongKeTable {
    private int idThongKeTable;
    private int idTable;
    private int countBill;

    public ThongKeTable() {
    }

    public ThongKeTable(int idThongKeTable, int idTable, int countBill) {
        this.idThongKeTable = idThongKeTable;
        this.idTable = idTable;
        this.countBill = countBill;
    }

    public int getIdThongKeTable() {
        return idThongKeTable;
    }

    public void setIdThongKeTable(int idThongKeTable) {
        this.idThongKeTable = idThongKeTable;
    }

    public int getIdTable() {
        return idTable;
    }

    public void setIdTable(int idTable) {
        this.idTable = idTable;
    }

    public int getCountBill() {
        return countBill;
    }

    public void setCountBill(int countBill) {
        this.countBill = countBill;
    }
    
}
