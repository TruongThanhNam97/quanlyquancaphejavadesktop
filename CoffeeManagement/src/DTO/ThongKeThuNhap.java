/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Nam
 */
public class ThongKeThuNhap 
{
    private int idThongKeThuNhap;
    private String date;
    private double thuNhap;

    public ThongKeThuNhap() {
    }

    public ThongKeThuNhap(int idThongKeThuNhap, String date, double thuNhap) {
        this.idThongKeThuNhap = idThongKeThuNhap;
        this.date = date;
        this.thuNhap = thuNhap;
    }

    public int getIdThongKeThuNhap() {
        return idThongKeThuNhap;
    }

    public void setIdThongKeThuNhap(int idThongKeThuNhap) {
        this.idThongKeThuNhap = idThongKeThuNhap;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getThuNhap() {
        return thuNhap;
    }

    public void setThuNhap(double thuNhap) {
        this.thuNhap = thuNhap;
    }
    
}
