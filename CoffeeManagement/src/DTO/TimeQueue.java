/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Nam
 */
public class TimeQueue {
    private int idQueue;
    private int idTable;
    private int timeQueue;

    public TimeQueue() {
    }

    public TimeQueue(int idQueue, int idTable, int timeQueue) {
        this.idQueue = idQueue;
        this.idTable = idTable;
        this.timeQueue = timeQueue;
    }

    public int getIdQueue() {
        return idQueue;
    }

    public void setIdQueue(int idQueue) {
        this.idQueue = idQueue;
    }

    public int getIdTable() {
        return idTable;
    }

    public void setIdTable(int idTable) {
        this.idTable = idTable;
    }

    public int getTimeQueue() {
        return timeQueue;
    }

    public void setTimeQueue(int timeQueue) {
        this.timeQueue = timeQueue;
    }
    
}
