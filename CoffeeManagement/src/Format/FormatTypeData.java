/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Format;

import java.sql.Timestamp;
import java.text.*;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author Nam
 */
public class FormatTypeData {
    
    private FormatTypeData() {
    }
    
    public static FormatTypeData getInstance() {
        return FormatTypeDataHolder.INSTANCE;
    }
    
    private static class FormatTypeDataHolder {

        private static final FormatTypeData INSTANCE = new FormatTypeData();
    }
    
    public String formatMoney(Object money)
    {
        String str="";
        DecimalFormat df = new DecimalFormat("#,###");
        DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.getDefault());
        dfs.setGroupingSeparator('.');
        df.setDecimalFormatSymbols(dfs);
        str = df.format(money);
        return str;
    }
    public java.sql.Date getDateTimeNow()
    {
        Calendar cal = Calendar.getInstance();
        Date d = cal.getTime();
        java.sql.Date dd = new java.sql.Date(d.getTime());
        return dd;
    }
    public Date getDateTimeToPrintBill()
    {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }
    public String formatDateTime(Date d)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(d);
    }
     public String formatDateTime1(Date d)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm aaa");
        return sdf.format(d);
    }
      public String formatDateTime2(Date d)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
        return sdf.format(d);
    }
    public String formatYear(java.sql.Date d)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        return sdf.format(d);
    }
    public String formatTime(Timestamp d)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aaa");
        return sdf.format(d);
    }
    public String formatTime1(Timestamp d)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        return sdf.format(d);
    }
    public String formatMinute(Date d)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        return sdf.format(d);
    }
    public String formatNumber(float f)
    {
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(f);
    }
}
