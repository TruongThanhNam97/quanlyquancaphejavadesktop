/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import BLL.DatabaseBLL;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import javax.swing.JOptionPane;

/**
 *
 * @author Nam
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static Properties pro;
    public static String pathConfigDatabase;
    public static void main(String[] args) 
    {
       pro = new Properties();
       pathConfigDatabase = System.getenv("localappdata")+"/infoConfigDatabaseProjectJava/infoDatabase.txt";
       try
       {
           File file1 = new File(System.getenv("localappdata")+"/infoConfigDatabaseProjectJava");
           if (file1.exists()==false)
           {
               file1.mkdirs();
           }
           File file2 = new File(pathConfigDatabase);
           if (file2.exists()==false)
           {
               file2.createNewFile();
           }
           File file3 = new File(System.getenv("localappdata")+"/image.txt");
           if (file3.exists()==false)
           {
               file3.createNewFile();
           }
           pro.load(new FileInputStream(pathConfigDatabase));
           if (pro.getProperty("databaseName")==null || pro.getProperty("databaseName").isEmpty())
           {
               CauHinhDatabase f = new CauHinhDatabase();
               f.show();
           }
           else
           {
               DatabaseBLL.getInformation(pro.getProperty("databaseName"),pro.getProperty("user"),pro.getProperty("pwd"));
               DangNhap f = new DangNhap();
               f.show();
           }
       }
       catch(Exception ex)
       {
           JOptionPane.showMessageDialog(null, ex.getMessage());
       }
    }    
}
