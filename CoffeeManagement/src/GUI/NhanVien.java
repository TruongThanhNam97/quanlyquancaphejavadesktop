/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import BLL.BillBLL;
import BLL.BillInfoBLL;
import BLL.FoodBLL;
import BLL.FoodcategoryBLL;
import BLL.TableBLL;
import BLL.TimeQueueBLL;
import DTO.BillInfo;
import DTO.Food;
import DTO.Foodcategory;
import DTO.Table;
import DTO.TimeQueue;
import Format.FormatTypeData;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Nam
 */
public class NhanVien extends javax.swing.JFrame {

    /**
     * Creates new form NhanVien
     */
    private Table selectedTable = null;
    private Food selectedFood = null;
    private double totalPrice;
    private Thread thread=null;
    private Timer timer = null;

    public NhanVien() {
        initComponents();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        firstDisplay();
        tDisplayBill.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (tDisplayBill.getSelectedRow() >= 0) 
                {
                    String count = tDisplayBill.getValueAt(tDisplayBill.getSelectedRow(),1).toString().trim();
                    txtFood.setText(tDisplayBill.getValueAt(tDisplayBill.getSelectedRow(), 0) + "");
                    nupEditCount.setValue(Integer.parseInt(count));
                    int idBill = BillBLL.getInstance().getIdBillForIdTableAndStatusBill(selectedTable.getIdTable());
                    int idFood = FoodBLL.getInstance().getIdFoodForNameFood(txtFood.getText());
                    txtNote.setText(BillInfoBLL.getInstance().getNoteIdBillAndIdFood(idBill, idFood));
                }
            }
        });
        tQueue.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (tQueue.getSelectedRow()>=0)
                {
                    
                }
            }
        });
        btnPay.setEnabled(false);
        btnXacNhanPhucVu.setEnabled(false);
        timer = new Timer(1000,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList<Table> listTablesMangVe = TableBLL.getInstance().getListTablesByStatusTable("Đang chờ phục vụ - Mang về");
                        DefaultTableModel model = new DefaultTableModel();
                        ArrayList<String> listColumns = new ArrayList<String>();
                        listColumns.add("Tên");
                        listColumns.add("Thời gian chờ");
                        model.setColumnIdentifiers(listColumns.toArray());
                        ArrayList<Object> listRows = new ArrayList<Object>();
                        for(Table x : listTablesMangVe)
                         {
                             int idBill = BillBLL.getInstance().getIdBillForIdTableAndStatusBill(x.getIdTable());
                             int timeNow = Integer.parseInt(FormatTypeData.getInstance().formatMinute(FormatTypeData.getInstance().getDateTimeToPrintBill()));
                             ArrayList<Timestamp> listTime = BillBLL.getInstance().getTimestampByIdBill(idBill);
                             int timeCheckIn = Integer.parseInt(FormatTypeData.getInstance().formatTime1(listTime.get(0)));
                             //listRows.clear();
                             //listRows.add(x.getNameTable()+" - Mang về");
                             int timeQueue = timeNow-timeCheckIn;
                             if (timeQueue>=0)
                                {
                                    //listRows.add("                    "+timeQueue+" phút");
                                    TimeQueueBLL.getInstance().insertData(x.getIdTable(),timeQueue);
                                }
                             else
                                {
                                    //listRows.add("                    "+(60-Math.abs(timeQueue))+" phút");
                                    TimeQueueBLL.getInstance().insertData(x.getIdTable(),60-Math.abs(timeQueue));
                                }
                             //model.addRow(listRows.toArray());
                         }
                        ArrayList<TimeQueue> listMangVes = TimeQueueBLL.getInstance().getDatas();
                        TimeQueueBLL.getInstance().deleteData();
                        for ( TimeQueue y : listMangVes)
                        {
                            listRows.clear();
                            listRows.add(TableBLL.getInstance().getTableForIdTable(y.getIdTable()).getNameTable()+" - Mang về");
                            listRows.add("                    "+y.getTimeQueue()+" phút");
                            model.addRow(listRows.toArray());
                        }
                        ArrayList<Table> listTablesNgoiQuan = TableBLL.getInstance().getListTablesByStatusTable("Đang chờ phục vụ - Ngồi quán");
                        for(Table x : listTablesNgoiQuan)
                         {
                             int idBill = BillBLL.getInstance().getIdBillForIdTableAndStatusBill(x.getIdTable());
                             int timeNow = Integer.parseInt(FormatTypeData.getInstance().formatMinute(FormatTypeData.getInstance().getDateTimeToPrintBill()));
                             ArrayList<Timestamp> listTime = BillBLL.getInstance().getTimestampByIdBill(idBill);
                             int timeCheckIn = Integer.parseInt(FormatTypeData.getInstance().formatTime1(listTime.get(0)));
                             int timeQueue = timeNow-timeCheckIn;
                             if (timeQueue>=0)
                                {
                                    TimeQueueBLL.getInstance().insertData(x.getIdTable(),timeQueue);
                                }
                             else
                                {
                                    TimeQueueBLL.getInstance().insertData(x.getIdTable(),60-Math.abs(timeQueue));
                                }
                         }
                        ArrayList<TimeQueue> listDatas = TimeQueueBLL.getInstance().getDatas();
                        TimeQueueBLL.getInstance().deleteData();
                        for(TimeQueue y : listDatas)
        {
            listRows.clear();
            listRows.add(TableBLL.getInstance().getTableForIdTable(y.getIdTable()).getNameTable()+" - Ngồi quán");
            listRows.add("                    "+y.getTimeQueue()+" phút");
            model.addRow(listRows.toArray());
        }
                        tQueue.setModel(model);
                    }
                });
                thread.start();
            }
        });
        timer.start();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        cboTable = new javax.swing.JComboBox<>();
        pnDisplayBill = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tDisplayBill = new javax.swing.JTable();
        pnGoiMon = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cboFoodcategory = new javax.swing.JComboBox<>();
        cboFood = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        nupCount = new javax.swing.JSpinner();
        btnXacNhan = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtNote = new javax.swing.JTextArea();
        pnEditCount = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtFood = new javax.swing.JTextField();
        nupEditCount = new javax.swing.JSpinner();
        btnEditCount = new javax.swing.JButton();
        pnPayBill = new javax.swing.JPanel();
        radNgoiQuan = new javax.swing.JRadioButton();
        radMangVe = new javax.swing.JRadioButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtTotalPrice = new javax.swing.JTextField();
        txtTienNhan = new javax.swing.JTextField();
        btnPay = new javax.swing.JButton();
        btnXacNhanPhucVu = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        nupSale = new javax.swing.JSpinner();
        btnDangKyThanhVien = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tQueue = new javax.swing.JTable();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        mnuExit = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocation(new java.awt.Point(811, 300));
        setSize(new java.awt.Dimension(1920, 1080));

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel1.setText("Danh mục số :");

        cboTable.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        cboTable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTableActionPerformed(evt);
            }
        });

        pnDisplayBill.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Thông tin hóa đơn", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 18))); // NOI18N

        tDisplayBill.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        tDisplayBill.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tDisplayBill);

        javax.swing.GroupLayout pnDisplayBillLayout = new javax.swing.GroupLayout(pnDisplayBill);
        pnDisplayBill.setLayout(pnDisplayBillLayout);
        pnDisplayBillLayout.setHorizontalGroup(
            pnDisplayBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnDisplayBillLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 756, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        pnDisplayBillLayout.setVerticalGroup(
            pnDisplayBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnDisplayBillLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );

        pnGoiMon.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Gọi món", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 18))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jLabel2.setText("Nhóm món ăn :");

        jLabel3.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jLabel3.setText("Món ăn :");

        cboFoodcategory.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        cboFoodcategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboFoodcategoryActionPerformed(evt);
            }
        });

        cboFood.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        cboFood.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboFoodActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jLabel4.setText("Ghi chú :");

        nupCount.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        nupCount.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                nupCountStateChanged(evt);
            }
        });

        btnXacNhan.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        btnXacNhan.setText("Xác nhận");
        btnXacNhan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXacNhanActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jLabel7.setText("Số lượng :");

        txtNote.setColumns(20);
        txtNote.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        txtNote.setLineWrap(true);
        txtNote.setRows(5);
        txtNote.setWrapStyleWord(true);
        jScrollPane2.setViewportView(txtNote);

        javax.swing.GroupLayout pnGoiMonLayout = new javax.swing.GroupLayout(pnGoiMon);
        pnGoiMon.setLayout(pnGoiMonLayout);
        pnGoiMonLayout.setHorizontalGroup(
            pnGoiMonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnGoiMonLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnGoiMonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(pnGoiMonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboFood, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pnGoiMonLayout.createSequentialGroup()
                        .addComponent(nupCount, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnXacNhan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cboFoodcategory, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnGoiMonLayout.setVerticalGroup(
            pnGoiMonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnGoiMonLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnGoiMonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboFoodcategory))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnGoiMonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboFood, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnGoiMonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnGoiMonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(nupCount, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(btnXacNhan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addGroup(pnGoiMonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnGoiMonLayout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnGoiMonLayout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );

        pnGoiMonLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cboFood, cboFoodcategory});

        pnEditCount.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Chỉnh sửa số lượng", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 18))); // NOI18N

        jLabel5.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jLabel5.setText("Món ăn :");

        jLabel6.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jLabel6.setText("Số lượng :");

        txtFood.setEditable(false);
        txtFood.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N

        nupEditCount.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        nupEditCount.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                nupEditCountStateChanged(evt);
            }
        });

        btnEditCount.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        btnEditCount.setText("Xác nhận");
        btnEditCount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditCountActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnEditCountLayout = new javax.swing.GroupLayout(pnEditCount);
        pnEditCount.setLayout(pnEditCountLayout);
        pnEditCountLayout.setHorizontalGroup(
            pnEditCountLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnEditCountLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnEditCountLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE))
                .addGap(81, 81, 81)
                .addGroup(pnEditCountLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnEditCountLayout.createSequentialGroup()
                        .addComponent(nupEditCount, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditCount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(txtFood))
                .addContainerGap())
        );
        pnEditCountLayout.setVerticalGroup(
            pnEditCountLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnEditCountLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnEditCountLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFood, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(pnEditCountLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnEditCountLayout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE))
                    .addGroup(pnEditCountLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnEditCountLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nupEditCount)
                            .addComponent(btnEditCount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );

        pnPayBill.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Thanh toán", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 18))); // NOI18N

        buttonGroup1.add(radNgoiQuan);
        radNgoiQuan.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        radNgoiQuan.setText("Ngồi quán");

        buttonGroup1.add(radMangVe);
        radMangVe.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        radMangVe.setText("Mang về");

        jLabel9.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jLabel9.setText("Tổng tiền :");

        jLabel10.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jLabel10.setText("Tiền nhận :");

        txtTotalPrice.setEditable(false);
        txtTotalPrice.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        txtTotalPrice.setForeground(new java.awt.Color(255, 0, 0));

        txtTienNhan.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        txtTienNhan.setForeground(new java.awt.Color(255, 0, 0));
        txtTienNhan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTienNhanKeyTyped(evt);
            }
        });

        btnPay.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        btnPay.setText("Thanh toán");
        btnPay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPayActionPerformed(evt);
            }
        });

        btnXacNhanPhucVu.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        btnXacNhanPhucVu.setText("Xác nhận đã phục vụ");
        btnXacNhanPhucVu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXacNhanPhucVuActionPerformed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jLabel11.setText("Giảm giá (%) :");

        nupSale.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        nupSale.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                nupSaleStateChanged(evt);
            }
        });

        btnDangKyThanhVien.setText("Đăng ký thành viên");
        btnDangKyThanhVien.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDangKyThanhVienActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnPayBillLayout = new javax.swing.GroupLayout(pnPayBill);
        pnPayBill.setLayout(pnPayBillLayout);
        pnPayBillLayout.setHorizontalGroup(
            pnPayBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnPayBillLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnPayBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnPayBillLayout.createSequentialGroup()
                        .addGroup(pnPayBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 149, Short.MAX_VALUE))
                        .addGap(40, 40, 40))
                    .addGroup(pnPayBillLayout.createSequentialGroup()
                        .addComponent(btnDangKyThanhVien)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)))
                .addGroup(pnPayBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnPayBillLayout.createSequentialGroup()
                        .addGroup(pnPayBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnXacNhanPhucVu, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nupSale, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(pnPayBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(btnPay, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(pnPayBillLayout.createSequentialGroup()
                                .addComponent(radNgoiQuan)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(radMangVe))))
                    .addGroup(pnPayBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(txtTienNhan, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 347, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtTotalPrice, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 347, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        pnPayBillLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnDangKyThanhVien, btnXacNhanPhucVu});

        pnPayBillLayout.setVerticalGroup(
            pnPayBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnPayBillLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnPayBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTotalPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnPayBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTienNhan, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnPayBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nupSale, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radNgoiQuan)
                    .addComponent(radMangVe))
                .addGap(34, 34, 34)
                .addGroup(pnPayBillLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnXacNhanPhucVu, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPay, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDangKyThanhVien))
                .addContainerGap(36, Short.MAX_VALUE))
        );

        pnPayBillLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txtTienNhan, txtTotalPrice});

        pnPayBillLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnDangKyThanhVien, btnPay, btnXacNhanPhucVu});

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Hàng chờ", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 14))); // NOI18N

        tQueue.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        tQueue.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(tQueue);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 421, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
        );

        jMenu1.setText("Thiết lập");

        mnuExit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        mnuExit.setText("Đăng xuất");
        mnuExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuExitActionPerformed(evt);
            }
        });
        jMenu1.add(mnuExit);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(pnEditCount, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnGoiMon, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 33, Short.MAX_VALUE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(cboTable, javax.swing.GroupLayout.PREFERRED_SIZE, 365, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(pnPayBill, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(29, 29, 29)
                .addComponent(pnDisplayBill, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnDisplayBill, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cboTable, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(pnGoiMon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnEditCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(pnPayBill, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(38, 38, 38))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nupCountStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_nupCountStateChanged
        // TODO add your handling code here:
        int x = (int) nupCount.getValue();
        if (x < 0) 
        {
            nupCount.setValue(0);
        }
    }//GEN-LAST:event_nupCountStateChanged

    private void nupEditCountStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_nupEditCountStateChanged
        // TODO add your handling code here:
        int x = (int) nupEditCount.getValue();
        if (x < 0) 
        {
            nupEditCount.setValue(0);
        }
    }//GEN-LAST:event_nupEditCountStateChanged

    private void nupSaleStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_nupSaleStateChanged
        // TODO add your handling code here:
        int x = (int) nupSale.getValue();
        if (x < 0) 
        {
            nupSale.setValue(0);
            x=0;
        }
        if (x>99)
        {
            nupSale.setValue(99);
            x=99;
        }
        if (!txtTotalPrice.getText().trim().isEmpty())
        {
            double newTotalPrice = (totalPrice*(100-x))/100;
            txtTotalPrice.setText(newTotalPrice+"");
        }
    }//GEN-LAST:event_nupSaleStateChanged

    private void mnuExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuExitActionPerformed
        // TODO add your handling code here:
        DangNhap f = new DangNhap();
        f.show();
        this.dispose();
    }//GEN-LAST:event_mnuExitActionPerformed

    private void cboFoodcategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboFoodcategoryActionPerformed
        // TODO add your handling code here:
        Foodcategory foodcategory = (Foodcategory) cboFoodcategory.getSelectedItem();
        ArrayList<Food> ds = FoodBLL.getInstance().getListFoodForIdFoodcategory(foodcategory.getIdFoodcategory());
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        for (Food x : ds) 
        {
            model.addElement(x);
        }
        cboFood.setModel(model);
        selectedFood = ds.get(0);
    }//GEN-LAST:event_cboFoodcategoryActionPerformed

    private void cboTableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTableActionPerformed
        // TODO add your handling code here:
        selectedTable = (Table) cboTable.getSelectedItem();
        pnDisplayBill.setBorder(new TitledBorder("Thông tin hóa đơn " + selectedTable.getNameTable()));
        pnGoiMon.setBorder(new TitledBorder("Gọi món " + selectedTable.getNameTable()));
        pnPayBill.setBorder(new TitledBorder("Thanh toán hóa đơn " + selectedTable.getNameTable()));
        pnEditCount.setBorder(new TitledBorder("Chỉnh sửa số lượng của hóa đơn " + selectedTable.getNameTable()));
        resetTxt();
        if(selectedTable.getStatusTable().equals("Trống"))
        {
            btnPay.setEnabled(false);
            btnXacNhanPhucVu.setEnabled(false);
            btnXacNhan.setEnabled(true);
            btnEditCount.setEnabled(true);
        }
        else if (selectedTable.getStatusTable().equals("Đang nhận yêu cầu"))
        {
            btnPay.setEnabled(true);
            btnXacNhan.setEnabled(true);
            btnEditCount.setEnabled(true);
            btnXacNhanPhucVu.setEnabled(false);
        }
        else
        {
            btnPay.setEnabled(false);
            btnXacNhanPhucVu.setEnabled(true);
            btnXacNhan.setEnabled(false);
            btnEditCount.setEnabled(false);
        }
        displayBillToTable();
    }//GEN-LAST:event_cboTableActionPerformed

    private void cboFoodActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboFoodActionPerformed
        // TODO add your handling code here:
        selectedFood = (Food) cboFood.getSelectedItem();
    }//GEN-LAST:event_cboFoodActionPerformed

    private void btnXacNhanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXacNhanActionPerformed
        // TODO add your handling code here:
        if (selectedTable == null) 
        {
            JOptionPane.showMessageDialog(null, "Chưa chọn số");
        } 
        else if (selectedFood == null) 
        {
            JOptionPane.showMessageDialog(null, "Chưa chọn món ăn");
        } 
        else 
        {
            int count = (int) nupCount.getValue();
            if (count == 0) 
            {
                JOptionPane.showMessageDialog(null, "Số lượng không hợp lệ");
            } 
            else 
            {
                String note = "";
                note = txtNote.getText().trim();
                if (selectedTable.getStatusTable().equals("Trống")) 
                {
                    BillBLL.getInstance().createBill(FormatTypeData.getInstance().getDateTimeNow(),FormatTypeData.getInstance().getDateTimeNow(), selectedTable.getIdTable());
                    int idBill = BillBLL.getInstance().getIdBillForIdTableAndStatusBill(selectedTable.getIdTable());
                    BillInfoBLL.getInstance().createBillInfo(idBill, selectedFood.getIdFood(), count, note);
                    TableBLL.getInstance().updateStatusTable("Đang nhận yêu cầu", selectedTable.getIdTable());
                    selectedTable = TableBLL.getInstance().getTableForIdTable(selectedTable.getIdTable());
                    btnPay.setEnabled(true);
                    resetTxt();
                    displayBillToTable();
                    displayCombobox();
                } 
                else if (selectedTable.getStatusTable().equals("Đang nhận yêu cầu")) 
                {
                    int idBill = BillBLL.getInstance().getIdBillForIdTableAndStatusBill(selectedTable.getIdTable());
                    if (BillInfoBLL.getInstance().isIdFoodExist(selectedFood.getIdFood(), idBill)) 
                    {
                        int idBillInfo = BillInfoBLL.getInstance().getIdBillInfoForIdBillAndIdFood(idBill, selectedFood.getIdFood());
                        int newCount = (int) nupCount.getValue() + BillInfoBLL.getInstance().getCountForIdBillAndIdFood(idBill, selectedFood.getIdFood());
                        BillInfoBLL.getInstance().updateCountForIdBillInfo(idBillInfo, newCount);
                        resetTxt();
                        displayBillToTable();
                        displayCombobox();
                    } 
                    else 
                    {
                        BillInfoBLL.getInstance().createBillInfo(idBill, selectedFood.getIdFood(), count, note);
                        resetTxt();
                        displayBillToTable();
                        displayCombobox();
                    }
                }
            }
        }
    }//GEN-LAST:event_btnXacNhanActionPerformed

    private void btnEditCountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditCountActionPerformed
        // TODO add your handling code here:
        if (txtFood.getText().trim().isEmpty())
        {
            JOptionPane.showMessageDialog(null, "Chưa có thông tin món ăn cần chỉnh sửa số lượng");
        }
        else
        {
            int idFood = FoodBLL.getInstance().getIdFoodForNameFood(txtFood.getText().trim());
            int idBill = BillBLL.getInstance().getIdBillForIdTableAndStatusBill(selectedTable.getIdTable());
            int idBillInfo = BillInfoBLL.getInstance().getIdBillInfoForIdBillAndIdFood(idBill, idFood);
            if ((int)nupEditCount.getValue()==0)
            {
                BillInfoBLL.getInstance().deleteBillInfo(idBillInfo);
                displayBillToTable();
                txtFood.setText("");
                txtNote.setText("");
            }
            else
            {
                BillInfoBLL.getInstance().updateCountForIdBillInfo(idBillInfo, (int)nupEditCount.getValue());
                displayBillToTable();
            }
        }
    }//GEN-LAST:event_btnEditCountActionPerformed

    private void btnXacNhanPhucVuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXacNhanPhucVuActionPerformed
        // TODO add your handling code here:
        int idBill = BillBLL.getInstance().getIdBillForIdTableAndStatusBill(selectedTable.getIdTable());
        TableBLL.getInstance().updateStatusTable("Trống", selectedTable.getIdTable());
        BillBLL.getInstance().updateDateCheckOut(idBill);
        BillBLL.getInstance().updateStatusBill(idBill);
        displayBillToTable();
        resetTxt();
        firstDisplay();
        selectedTable=null;
    }//GEN-LAST:event_btnXacNhanPhucVuActionPerformed

    private void btnPayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPayActionPerformed
        // TODO add your handling code here:
        if (txtTienNhan.getText().trim().isEmpty())
        {
            JOptionPane.showMessageDialog(null, "Chưa nhập tiền nhận");
        }
        else if (radNgoiQuan.isSelected()==false && radMangVe.isSelected()==false)
        {
            JOptionPane.showMessageDialog(null, "Chưa có thông tin khách ngồi lại quán hay mang về");
        }
        else
        {
            double tienNhan = Double.parseDouble(txtTienNhan.getText().trim());
            double tongTien = Double.parseDouble(txtTotalPrice.getText().trim());
            int sale = (int)nupSale.getValue();
            double tienThoi = tienNhan-tongTien;   
            int idBill = BillBLL.getInstance().getIdBillForIdTableAndStatusBill(selectedTable.getIdTable());
            if (tienThoi>=0)
            {
                BillBLL.getInstance().updateTienNhanTienThuaSale(tienNhan, tienThoi, sale, idBill);
                if (radNgoiQuan.isSelected())
                {
                   TableBLL.getInstance().updateStatusTable("Đang chờ phục vụ - Ngồi quán", selectedTable.getIdTable());
                }
                else
                {
                   TableBLL.getInstance().updateStatusTable("Đang chờ phục vụ - Mang về", selectedTable.getIdTable());
                }
                displayBillToTable();
                BillBLL.getInstance().updateDateCheckIn(idBill);
                btnXacNhanPhucVu.setEnabled(true);
                btnPay.setEnabled(false);
                btnXacNhan.setEnabled(false);
                btnEditCount.setEnabled(false);
                resetTxt();
                firstDisplay();
                radNgoiQuan.setSelected(false);
                radMangVe.setSelected(false);
                
                ArrayList<Map<String,?>> dataSource = new ArrayList<Map<String, ?>>();
                ArrayList<BillInfo> ds = BillInfoBLL.getInstance().getListBillInfoForIdBill(idBill);
            for(BillInfo x : ds)
            {
                Map<String,Object> m1 = new HashMap<String,Object>();
                m1.put("timeNow", FormatTypeData.getInstance().formatDateTime1(FormatTypeData.getInstance().getDateTimeToPrintBill()));
                m1.put("thuNgan",DangNhap.selectedAccount.getName());
                m1.put("nameTable",selectedTable.getNameTable());
                m1.put("totalPrice",FormatTypeData.getInstance().formatMoney(totalPrice));
                m1.put("sale", sale+"");
                m1.put("thanhToan",FormatTypeData.getInstance().formatMoney(tongTien));
                m1.put("tienNhan", FormatTypeData.getInstance().formatMoney(tienNhan));
                m1.put("tienThoi", FormatTypeData.getInstance().formatMoney(tienThoi));
                m1.put("nameFood",FoodBLL.getInstance().getNameFoodForIdFood(x.getIdFood()));
                m1.put("count",x.getCount()+"");
                m1.put("price", FormatTypeData.getInstance().formatMoney(FoodBLL.getInstance().getPriceFoodForIdFood(x.getIdFood())));
                m1.put("priceFood",FormatTypeData.getInstance().formatMoney(x.getCount()*FoodBLL.getInstance().getPriceFoodForIdFood(x.getIdFood())));
                dataSource.add(m1);
            }
            try
            {
            JRDataSource jrDataSource = new JRBeanCollectionDataSource(dataSource);
            String sourceName= "src/GUI/bill.jrxml";
            String reportDest ="src/GUI/bill.jasper";
            JasperReport report = JasperCompileManager.compileReport(sourceName);
            JasperPrint filledReport = JasperFillManager.fillReport(report, null,jrDataSource);
            JasperExportManager.exportReportToHtmlFile(filledReport,reportDest);
            JasperViewer.viewReport(filledReport,false);
            }
            catch(Exception ex)
            {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
                
            }
            else
            {
                JOptionPane.showMessageDialog(null, "Chưa đủ tiền thanh toán");
            }
        }   
    }//GEN-LAST:event_btnPayActionPerformed

    private void btnDangKyThanhVienActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDangKyThanhVienActionPerformed
        // TODO add your handling code here:
        CreateKhachHang f = new CreateKhachHang();
        f.show();
    }//GEN-LAST:event_btnDangKyThanhVienActionPerformed

    private void txtTienNhanKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTienNhanKeyTyped
        // TODO add your handling code here:
        if (Character.isDigit(evt.getKeyChar())==false && Character.isISOControl(evt.getKeyChar())==false )
            {
                evt.consume();
            }
    }//GEN-LAST:event_txtTienNhanKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NhanVien.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NhanVien.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NhanVien.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NhanVien.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NhanVien().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDangKyThanhVien;
    private javax.swing.JButton btnEditCount;
    private javax.swing.JButton btnPay;
    private javax.swing.JButton btnXacNhan;
    private javax.swing.JButton btnXacNhanPhucVu;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> cboFood;
    private javax.swing.JComboBox<String> cboFoodcategory;
    private javax.swing.JComboBox<String> cboTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JMenuItem mnuExit;
    private javax.swing.JSpinner nupCount;
    private javax.swing.JSpinner nupEditCount;
    private javax.swing.JSpinner nupSale;
    private javax.swing.JPanel pnDisplayBill;
    private javax.swing.JPanel pnEditCount;
    private javax.swing.JPanel pnGoiMon;
    private javax.swing.JPanel pnPayBill;
    private javax.swing.JRadioButton radMangVe;
    private javax.swing.JRadioButton radNgoiQuan;
    private javax.swing.JTable tDisplayBill;
    private javax.swing.JTable tQueue;
    private javax.swing.JTextField txtFood;
    private javax.swing.JTextArea txtNote;
    private javax.swing.JTextField txtTienNhan;
    private javax.swing.JTextField txtTotalPrice;
    // End of variables declaration//GEN-END:variables

    private void displayCombobox() {
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        ArrayList<Table> ds = TableBLL.getInstance().displayCombobox();
        for (Table x : ds) 
        {
            model.addElement(x);
        }
        cboTable.setModel(model);
    }
    private void displayComboboxFoodcategory() {
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        ArrayList<Foodcategory> ds = FoodcategoryBLL.getInstance().displayCombobox();
        for (Foodcategory x : ds) 
        {
            model.addElement(x);
        }
        cboFoodcategory.setModel(model);
    }

    public void firstDisplay() {
        displayCombobox();
        displayComboboxFoodcategory();
    }

    private void displayBillToTable() {
        totalPrice=0;
        int idBill = BillBLL.getInstance().getIdBillForIdTableAndStatusBill(selectedTable.getIdTable());
        ArrayList<BillInfo> ds = BillInfoBLL.getInstance().getListBillInfoForIdBill(idBill);
        DefaultTableModel model = new DefaultTableModel();
        ArrayList<String> listColumns = new ArrayList<String>();
        listColumns.add("Tên món ăn");
        listColumns.add("Số lượng");
        listColumns.add("Đơn giá");
        listColumns.add("Ghi chú");
        model.setColumnIdentifiers(listColumns.toArray());
        ArrayList<Object> listRows = new ArrayList<Object>();
        for (BillInfo x : ds) 
        {
            listRows.clear();
            listRows.add(FoodBLL.getInstance().getNameFoodForIdFood(x.getIdFood()));
            listRows.add("                    "+x.getCount());
            listRows.add("                 "+FormatTypeData.getInstance().formatMoney(FoodBLL.getInstance().getPriceFoodForIdFood(x.getIdFood())));
            listRows.add(x.getNote());
            model.addRow(listRows.toArray());
            totalPrice+=x.getCount()*FoodBLL.getInstance().getPriceFoodForIdFood(x.getIdFood());
        }
        tDisplayBill.setModel(model);
        txtTotalPrice.setText(totalPrice+"");
        BillBLL.getInstance().updateTotalPrice(totalPrice, idBill);
    }

    private void resetTxt() {
        txtNote.setText("");
        txtFood.setText("");
        nupCount.setValue(0);
        nupEditCount.setValue(0);
        txtTotalPrice.setText("");
        txtTienNhan.setText("");
        nupSale.setValue(0);
    }
}
